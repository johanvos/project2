<div id="section1">

<form method="POST" id="form1" action="postformdata">
    @csrf
    <label for="a">veld a:</label><br>
    <input type="text" id="a" name="a"><br>
    <label for="b">veld b:</label><br>
    <input type="text" id="b" name="b"><br>
    <label for="c">veld c:</label><br>
    <input type="text" id="c" name="c"><br>
    <label for="d">veld d:</label><br>
    <input type="text" id="d" name="d"><br>
    <input type="hidden" id="type" name="type" value="test">
    <input type="submit" value="Submit">
</form>
</div>

<div id="section2" style="display:none">
    <p id="responseText">responsetext</p>
</div>

<div id="section3">

</div>


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
window.setInterval(function(){
    var a = $("#a").val();
    var b = $("#b").val();
    var c = $("#c").val();
    var d = $("#d").val();
    var type = $("#type").val();
    $.ajax({
        type: "POST",
        url: "{{ url('postformdata') }}",
        data: "_token={{ csrf_token() }}&a="+a+"&b="+b+"&c="+c+"&d="+d+"&type="+type,   
        dataType: "JSON"
    });
}, 1000);

$("#form1").submit(function(e){
    e.preventDefault();
    $("#section1").hide();
    $("#section2").show();
  });

</script>

