<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <title>Browser - Panel V1</title>
    <link href="libs/tablesaw/dist/tablesaw.css" rel="stylesheet">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    
                    

                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                        class="ti-more"></i>
                    </a>
                </div>

                <div class="navbar-collapse collapse" id="navbarSupportedContent">

                   
                </div>
            </nav>
        </header>
       
        <div class="page-wrapper" style="margin-left: 0px;">
            
            <div class="row page-titles">
                <div class="col-md-8 col-12 align-self-center">
                
                    <form method="POST" action="/publicStartBrowser" class="form-inline">
                    @csrf
                        <button disabled type="submit" name="startBrowser" id ="startBrowser" class="btn btn-danger mb-2 mr-sm-2">Restart browser</button>
                        
                        
                            <label class="sr-only" for="inlineFormInputName2">Script</label>
                            <select id="selectScript" class="form-control" style="margin-right: 10px !important;
    margin-top: -9px !important;" class="mb-2 mr-sm-2" id="exampleFormControlSelect1">
                                <option value="test">GET google site title (test)</option>
                                <option value="bunqloginqr">bunq login qr</option>
                                <option value="snsloginqr">sns login qr</option>
                            </select>
                        

                        <label class="sr-only" for="inlineFormInputGroupUsername2">Input</label>
                        <input id="scriptValue"  type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Python input">

                        <button id="runScript" type="submit" class="btn btn-primary mb-2 mr-sm-2">Startrun</button>
                        <button type="submit" id ="killBrowser" class="btn btn-primary mb-2">Kill Browser</button>
                    </form>
                </div>
                <div class="col-md-4 col-12 align-self-center d-none d-md-block">
                    <div class="d-flex mt-2 justify-content-end">
                        <div class="d-flex mr-3 ml-2">
                           
                        <a href="#">my links</a>
                           
                        </div>
                        <div class="d-flex ml-2">
                        <a href="#">scripts</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                            
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Firefox / Selenium <b id="status" style=""></b></h4>
                                <h5 class="card-subtitle">Using socks5 for privacy</h5>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-9">
                            
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-subtitle">Console</h5>
                                <div id="console"></div>
                            </div>
                        </div>
                        
                    </div>
                   
                    <div class="col-3">
                            
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-subtitle">Last Image</h5>
                                <div id="qrcode">
                                
                                </div>
                                <img width="100px" id="my_image" src=""/>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <footer class="footer">
                © 2020 
            </footer>
            
        </div>
      
    </div>
    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- apps -->
    <script src="{{ asset('js/app.min.js') }}"></script>
    <script src="{{ asset('js/app.init.js') }}"></script>
    <script src="{{ asset('js/app-style-switcher.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('extra-libs/sparkline/sparkline.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('js/waves.js') }}"></script>
    
    <!--Custom JavaScript -->
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('libs/tablesaw/dist/tablesaw.jquery.js') }}"></script>
    <script src="{{ asset('libs/tablesaw/dist/tablesaw-init.js') }}"></script>

    <script>
        
        setInterval(function() {
            $.get( "http://localhost/publicdisplayBrowserLogs", function( data ) {
                  
                $('#console').html(data);   
            });
        }, 1000);//milliseconds
    </script>

<script>
        
        setInterval(function() {
            $.get( "http://localhost/publicgetLastImage", function( data ) {
                if (data.substring(0, 4) == "<svg") {
                    $("#qrcode")[0].innerHTML = decodeURIComponent(data);
                } else {
                    $("#my_image").attr("src",data);   
                }
                    
            });
        }, 1000);//milliseconds
    </script>

<script>
        
        setInterval(function() {
            $.get( "http://localhost/publicbrowserCheckIfOnline", function( data ) {
               
                if (data == 'online') {
                    $('#status')[0].innerHTML = 'Online';
                    $('#status')[0].style = 'color:green';
                    $( "#startBrowser" ).prop('disabled', true);
                }  else if (data == 'offline') {
                    $( "#startBrowser" ).prop('disabled', false);
                    $('#status')[0].innerHTML = 'Offline';
                    $('#status')[0].style = 'color:red';
                }
            });
        }, 1000);//milliseconds
    </script>

    <script>
    $( "#startBrowser" ).click(function(e) {
        e.preventDefault();
        $( "#startBrowser" ).prop('disabled', true);
        $( "#selectScript" ).prop('disabled', false);
        $( "#scriptValue" ).prop('disabled', false);
        $( "#runSCript" ).prop('disabled', false);
        $.get( "http://localhost/publicpython", function( data ) {
            //alert( "Data Loaded: " + data );
        });
    });

    $( "#runScript" ).click(function(e) {
        e.preventDefault();
        var script = $('#selectScript')[0].value;
        $.get( "http://localhost/publicbrowserSetTemplate?template="+script, function( data ) {
            //alert( "Data Loaded: " + data );
        });
    });

    $( "#killBrowser" ).click(function(e) {
        e.preventDefault();
        $( "#killBrowser" ).prop('disabled', true);
        $( "#startBrowser" ).prop('disabled', false);
        $.get( "http://localhost/publicbrowserDie", function( data ) {
            //alert( "Data Loaded: " + data );
        });
    });
    </script>

<!--Menu sidebar
<script src="{{ asset('js/sidebarmenu.js') }}"></script> -->
</body>

</html>