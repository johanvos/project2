<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

@if(session()->has('username')) 

<div class="row">
    <div class="col-12 text-right p-4">
    <a href="{{ url('logout') }}">logout</a>
    </div>
    </div>
@else
    <script>window.location = "/public/login";</script>
@endif

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


<body>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Controls</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><b>Bank: </b>Bunq</p>
        <p><b>Gebruikersnaam: </b>NL04 BUNQ 0392 3918 29</p>
        <p><b>Emailadres: </b>test@email.com</p>
        <p><b>Wachtwoord: </b>T123@)1123</p>
        <label for="responseText">Response 1:</label><br>
        <input type="text" id="responseText" class="form-control" placeholder="Vul een response in" name="responseText">
        <button type="button" class="mt-2 btn btn-secondary" data-dismiss="modal">Versturen</button>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


<div class="row mt-5">
    <div class="col-12">
        <form>
        <div class="form-row">
           
            <div class="col">
                <input type="text" id="copytxt" class="form-control" placeholder="Klik op een id" name="copytxt">
            </div>
            <div class="col">
            <input type ="text" id="copytxt" class="form-control" placeholder="Vul de code in" name="copytxt">
            </div>
            <div class="col">
            <input type="text" class="form-control" placeholder="Of de afbeeldings url">
            </div>
            <input type="hidden" id="bank" class="form-control" placeholder="bank" name="bank">
            <div class="col">
            <input type="submit" id="submitBtn" value="Go">
            <input type="submit" id="submitBtn" value="Afbeelding ophalen">
            </div>
        </div>
        </form>
    </div>
    <div class="col-12 mt-5">
        <table id="example" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>id</th>
                    <th>type</th>
                    <th>ff1</th>
                    <th>ff2</th>
                    <th>ff3</th>
                    <th>ff4</th>
                    <th>startTime</th>
                    <th>endTime</th>
                    <th>page</th>
                    
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    <div>
</div>


<script>
var lasttime = '';
function makeTr(row)
{
    lasttime = row['lastTime'];
    if(row['lastTime'] == lasttime) {
        console.log('offline');
    } else {
        console.log('online');
    }

    
    
    rowbody = "<td><p data-toggle='modal' data-target='#exampleModal' class='"+ row['type'] + "' id='"+ row['id'] + "' onClick='copyText(this)'>"+ row['id'] + "</p></td><td>"+ row['type'] + "</td><td>"+ row['ff1'] + "</td><td>"+ row['ff2'] + "</td><td>"+ row['ff3'] + "</td><td>"+ row['ff4'] + "</td><td>"+ row['startTime'] + "</td><td>"+ row['endTime'] + "</td><td></td>";
    markup = "<tr id="+row['id']+">"+rowbody+"</tr>"; 

    if($("#" + row['id']).length == 0) {
        tableBody = $("table tbody"); 
        tableBody.append(markup); 
    } else {
        $("#" + row['id']).replaceWith(markup);
    }
}

function updateTable()
{
    $.getJSON( "{{ url('printJson') }}", function( json ) {
        $.each(json, function(index, row) {
            console.log(row['id']);
            //console.log(row['type']);
            //console.log(row['startTime']);
            //console.log(row['endTime']);
            makeTr(row);
        });
    });
}

var a = '';
function copyText(element) {
 a = element;
  var text = element.innerText;
  var type = element.className;
  $('#copytxt').val(text);
  $('#bank').val(type);
}

updateTable();

$( document ).ready(function() {
    setInterval(updateTable,1000);
});

</script>

</body>

