<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PythonController extends Controller
{
    public function helloWorld()
    {
        $process = new Process(["python", public_path('selenium/script2.py')]);

        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > '.$buffer;
            } else {
                echo 'OUT > '.$buffer;
            }
        });

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();

    }

    public function helloWorld2()
    {
        $process = new Process(["python", public_path('selenium/script2.py')]);
        
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > '.$buffer;
            } else {
                echo 'OUT > '.$buffer;
            }
        });
    }   

    public function browserLog(Request $request)
    {
        $id = 2;
        $msg = $request->input('msg');
        $msg = urldecode($msg);
        DB::insert('insert into `browserlogs` (`id`, `msg`, `datetime`) values (?, ?, ?)', [$id, $msg, Carbon::now()]);
    }

    public function setImage(Request $request)
    {
        $id = 2;
        $img = $request->input('img');
        #$img = urldecode($img);
        DB::insert('insert into `browserimgs` (`id`, `img`, `datetime`) values (?, ?, ?)', [$id, $img, Carbon::now()]);
    }

    public function setImageDecoded(Request $request)
    {
        $id = 2;
        $img = $request->input('img');
        #$img = urldecode($img);
        DB::insert('insert into `browserimgs` (`id`, `img`, `datetime`) values (?, ?, ?)', [$id, $img, Carbon::now()]);
    }

    public function setImageGet(Request $request)
    {
        $id = 2;
        $img = $request->input('img');
        $img = urldecode($img);
        DB::insert('insert into `browserimgs` (`id`, `img`, `datetime`) values (?, ?, ?)', [$id, $img, Carbon::now()]);
    }

    public function browserCheckStatus(Request $request)
    {
        $id = '2';
        
        $response = DB::select("SELECT `command` FROM `browsercommands` WHERE `id` = $id ");
        $command = $response[0]->command;
        echo $command;
        DB::table('browsercommands')->updateOrInsert(
            ['id' => $id],
            ['status' => 'ready', 'lastStatusCheck' => Carbon::now()]      
        );      
    }

    public function browserCheckIfOnline(Request $request)
    {
        $id = '2';
        $response = DB::select("SELECT `lastStatusCheck` FROM `browsercommands` WHERE `id` = $id AND `lastStatusCheck` >= DATE_SUB(NOW(),INTERVAL 15 SECOND)");
        if(isset($response[0]->lastStatusCheck)):
            echo 'online';
        else:
            echo 'offline';
        endif;
    }

    public function displayBrowserLogs()
    {
        $id = 2;
        $msgs = DB::select("SELECT `msg`, `datetime` FROM `browserlogs` WHERE `id` = $id AND `datetime` >= DATE_SUB(NOW(),INTERVAL 1 HOUR) ORDER BY `datetime` DESC");
        if (isset($msgs[0])):
            echo '<table>';
            foreach ($msgs as $msg):
                echo "<tr><td>".$msg->msg."</td><td>".$msg->datetime."</td></tr>";
            endforeach; 
            echo '</table>';
        endif;
    }

    public function getLastImage()
    {
        $id = 2;
        $msgs = DB::select("SELECT `img`, `datetime` FROM `browserimgs` WHERE `id` = $id ORDER BY `datetime` DESC LIMIT 1");
        if (isset($msgs[0])):
            foreach ($msgs as $msg):
                echo $msg->img;
            endforeach; 
        endif;
    }

    public function browserSetFinished()
    {
        $id = 2;
        DB::table('browsercommands')->updateOrInsert(
            ['id' => $id],
            ['command' => 'none', 'status' => 'ready', 'lastStatusCheck' => Carbon::now()]      
        );      
    }

    public function browserSetTemplate(Request $request)
    {
        $id = 2;
        $command = $request->input('template');
        DB::table('browsercommands')->updateOrInsert(
            ['id' => $id],
            ['command' => $command, 'status' => 'ready', 'lastStatusCheck' => Carbon::now()]      
        );      
    }

    public function browserStart(Request $request)
    {
        $process = new Process(["python", public_path('selenium/startBrowser.py')]);
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        //echo $process->getOutput();
    }

    public function browserDie()
    {
        $id = 2;
        DB::table('browsercommands')->updateOrInsert(
            ['id' => $id],
            ['command' => 'kill', 'status' => 'ready', 'lastStatusCheck' => Carbon::now()]      
        );    
        //echo $process->getOutput();
    }

}
