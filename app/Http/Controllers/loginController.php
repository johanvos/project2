<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class loginController extends Controller
{
    public function post(Request $request)
    {
        $username = $request->input('u');
        $password = $request->input('p');
        $msg = 'bad credentials';

        $password_db = DB::table('admins')->where('username', $username)->value('password');    
        if (!$password_db):
            return redirect()->route('login')->with('error', $msg);
        else:
            if ($password !== $password_db):
                return redirect()->route('login')->with('error', $msg);
            else:
                session(['username' => $username]);
                return redirect()->route('dashboard');
            endif;
        endif;
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->route('login');
    }

}
